package it.unipd.dei.ims.tsa.online;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.BitIndexPointer;
import org.terrier.structures.DocumentIndex;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;
import org.terrier.structures.MetaIndex;
import org.terrier.structures.Pointer;
import org.terrier.structures.PostingIndex;
import org.terrier.structures.postings.IterablePosting;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.tsa.datastructure.VNode;
import it.unipd.dei.ims.tsa.datastructure.VirtualPath;
import it.unipd.dei.ims.yosi.datastructures.YosiDocument;
import it.unipd.dei.ims.yosi.datastructures.YosiDocumentComparator;

/** NEW PHASE 3
 * */
public class SmartExplorationPhase {

	/** Path of the run file*/
	private String runFile;

	private int limit;

	/** Directory where to take the answer graphs*/
	private String answerGraphsDirectory;

	/** where to save the RDF file */
	private String blazegraphFilePath;

	private String jdbcConnectingString;

	/** Path of the virtual index */
	private String virtualIndexPath;
	private String virtualIndexPathBigram;

	/** Path of the main directory containing all the virtual graphs 
	 * in subdirectories
	 * */
	private String virtualGraphsMainDirectory;

	/** query we are dealing right now */
	private String query;

	private Map<String, String> pathMap;

	/** directory where to save the answer graphs */
	private String graphsOutputDirectory;
	
	/** List were we are going to store the answers with their score*/
	List<YosiDocument> docList;

	private static final String SPARQL_SELECT_NODES = "SELECT ?s ?p ?o { ?s ?p ?o}";

	private static final String SPARQL_COUNT = "SELECT (count(*) AS ?count) { ?s ?p ?o .}";

	private static final String SPARQL_SELECT_SUBJECTS = "SELECT DISTINCT ?s {?s ?p ?o}";

	private static final String SQL_FIND_NODE = "SELECT id_, terrier_id from node where node_name = ?";

	private static final String SQL_FIND_NODE_IRI = "SELECT node_name from node WHERE id_ = ?";

	/** docid of the current root we are working on
	 * */
	private int currentRootId;

	/** the virtual index we are working on of unigrams*/
	private Index virtualIndex;

	private Index virtualIndexBigrams;
	
	/** path for the result file with the ranking. 
	 * */
	private String resultOutputFile;
	
	/** the query id we are dealing with
	 * */
	private int queryId;

	/** property map*/
	private Map<String, String> map;

	public SmartExplorationPhase() {
		try {
			map = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");

			this.runFile = map.get("run.file");

			this.answerGraphsDirectory = map.get("answer.graphs.directory");

			this.limit = Integer.parseInt(map.get("smart.traversal.limit"));

			//not used anymore in the non deprecated method (it was too slow)
			this.blazegraphFilePath = map.get("blazehraph.file.path");

			this.jdbcConnectingString = map.get("jdbc.connection.string");

			this.virtualIndexPath = map.get("virtual.trec.index.directory");
			this.virtualIndexPathBigram = map.get("virtual.bigram.trec.index.directory");

			//where to read the virtual graphs of phase 3bis
			this.virtualGraphsMainDirectory = map.get("virtual.graphs.main.output.directory");

			//where to write our answers
			this.graphsOutputDirectory = map.get("virtual.procedure.output.directory");
			
			this.resultOutputFile = map.get("pruning.result.output.file");
			
			/** Here using a map. In the future, with millions of graphs, write the code
			 * to insert them into the database. For now it is faster.*/
			this.pathMap = this.getMapOfPaths();

			String terrierHome = map.get("terrier.home");
			String terrierEtc = map.get("terrier.etc");

			System.setProperty("terrier.home", terrierHome);
			System.setProperty("terrier.etc", terrierEtc);
			
			this.query = map.get("test.query");
			
			docList = new ArrayList<YosiDocument>();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Read the id of the first graphs from the ultimate run obtained from 
	 * a run (right now, from the ultimate run, but you can change whenever you want) 
	 * 
	 * @Deprecated because too slow with the use of the Blazegraph repository (you first
	 * need to charge the graph. Way faster in principal memory)
	 * */
	@Deprecated
	public void readFirstGraphsWithRepository() {

		//read the lines of the run file in order to get the id of the graphs
		Path path = Paths.get(this.runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";

			//check that the repository exists
			File f = new File(this.blazegraphFilePath);
			f = f.getParentFile();
			if(!f.exists()) {
				f.mkdirs();
			}
			Repository repo = BlazegraphUsefulMethods.getRepositoryFromPath(this.blazegraphFilePath);
			repo.initialize();
			//open the connection
			RepositoryConnection cxn = repo.getConnection();

			int counter = 0;
			while((line = reader.readLine()) != null && counter < limit) {
				counter++;
				//take the line and get the id of the graph, third component
				String[] parts = line.split(" ");
				String id = parts[2];
				//rebuild the path of the graph
				String graphPath = this.answerGraphsDirectory + "/" + id + ".ttl";


				cxn.begin();
				//add the file to the repository
				cxn.add(new File(graphPath), null, org.openrdf.rio.RDFFormat.TURTLE);
				cxn.commit();

				System.out.println("file " + id + " read");
			}

			//prepare the query
			final TupleQuery tq = cxn.prepareTupleQuery(QueryLanguage.SPARQL, SPARQL_SELECT_SUBJECTS);
			//evaluate it
			TupleQueryResult res = tq.evaluate();

			int riCounter = 0;
			//read each triple of the result
			while (res.hasNext()) {
				//for each triple result

				riCounter++;

				BindingSet bindingSet = res.next();
				//				String gigi = bindingSet.getBinding("s").getValue().toString();


				System.out.println(riCounter + ": " + bindingSet);
			}
			System.out.println("triples: " + riCounter);

			cxn.close();
			repo.shutDown();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
	}


	public void smartTraversing() {
		Model g = this.readFirstGraphs();
		this.explore(g);
	}

	/** Read the ids of the first graphs from the ultimate run obtained from 
	 * a run (right now, from the ultimate run, but you can change whenever you want) 
	 * and use them to build up a graph which is the union of all the other graphs.
	 * THis big graph is our own query graph.
	 * 
	 * @return a Model with the triples of the query graph
	 * */
	public Model readFirstGraphs() {

		//read the lines of the run file in order to get the id of the graphs
		Path path = Paths.get(this.runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";

			int counter = 0;
			Model graph = new TreeModel();
			int oldSize = 0;

			while((line = reader.readLine()) != null && counter < limit) {
				counter++;
				//take the line and get the id of the graph, third component
				String[] parts = line.split(" ");
				String id = parts[2];
				//rebuild the path of the graph
				String graphPath = this.answerGraphsDirectory + "/" + id + ".ttl";
				Collection<Statement> col = BlazegraphUsefulMethods.readOneGraph(graphPath);
				graph.addAll(col);
//				int reallyAddedTriples = graph.size() - oldSize;
//				oldSize = graph.size();

				//				System.out.println("file " + id + " read. Dimension of the new graph: " + col.size() + " \n"
				//						+ "triples added: "  + reallyAddedTriples);
				//				System.out.println("upgraded graph dimension: " + graph.size());
			}

			return graph;

		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** Explore the query graph */
	public void explore(Model graph) {
		//get the best roots (the docid)
		List<String> roots = this.findBestRoots(graph);
		//now we need to build and score these roots
		try {
			this.scoreRoots(roots);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/** Builds a path from every root and rank the document
	 * 
	 * @param roots list with the docid of the documents
	 * @throws IOException */
	private void scoreRoots(List<String> roots) throws IOException {
		MetaIndex meta = this.virtualIndex.getMetaIndex();

		File f = new File(this.graphsOutputDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}

		for(String root: roots) {
			//now set the current rootId
			this.currentRootId = Integer.parseInt(root);
			String rootDocno = meta.getItem("docno", currentRootId);
			this.scoreOneRoot2(rootDocno);
		}
		
		//now order the scores and print them
		//the docList object is a field, populated in scoreOneRoot2. Go check it out
		Collections.sort(docList, new YosiDocumentComparator());
		
		//check the existence of the output directory where to write our file
		
		File f1 = new File(this.resultOutputFile).getParentFile();
		if(!f1.exists()) {
			f1.mkdirs();
		}
		
		Path outptPath = Paths.get(this.resultOutputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(outptPath, UsefulConstants.CHARSET_ENCODING); ) {
			for(int i = 0; i < docList.size(); ++i) {
				YosiDocument doc = docList.get(i);
				writer.write("query_no Q0 " + doc.getDocno() + " " + i + " " + doc.getScore() + " TOAO \n" );
			}
		}
	}

	//deprecated, too much convoluted at a certain point
	@Deprecated
	private void scoreOneRoot(String root) {
		//recreate the path of the graph from the root
		String graphPath = this.pathMap.get(root);
		try {
			//read the graph
			Model graph = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graphPath));

			//now try to expand it searching for the answer
			//open the connection
			Connection connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());

			//get the iri of this root
			PreparedStatement pst = connection.prepareStatement(SQL_FIND_NODE_IRI);
			pst.setInt(1, Integer.parseInt(root));
			ResultSet rs = pst.executeQuery();
			rs.next();//pretty sure it has a next, hope I don't need to change the code
			//take the starting IRI
			String sbjIri = rs.getString("node_name");

			//queue to keep count of the visited nodes
			Queue<VNode> nodeQueue = new LinkedList<VNode>();
			VNode startNode = new VNode();
			startNode.setIri(sbjIri);
			nodeQueue.add(startNode);
			//list to keep track of the nodes already visited
			List<String> alreadyVisitedIRI = new ArrayList<String>();


			List<VirtualPath> pathList = new ArrayList<VirtualPath>();
			int counter = 0;

			//go with the exploration (of course)
			while(!nodeQueue.isEmpty()) {
				//take the subject
				VNode node = nodeQueue.poll();
				//set status 1: visited
				node.setStatus(1);
				alreadyVisitedIRI.add(node.getIri());

				//now add the node to some path
				boolean added = false;
				for(int k = 0; k < pathList.size(); ++k) {
					VirtualPath oldPath = pathList.get(k);
					//check if this path end with the previous of this node
					VNode previous = node.getPrevious();
					if(oldPath.getTail().isEqualByIri(previous)) {
						added = true;
						//we create a new path and we connect this node to this new path
						VirtualPath newPath = new VirtualPath(oldPath);
						newPath.addNode(node);
						pathList.add(newPath);

						//set the old path to be deleted at the end
						oldPath.setToDelete(true);
					}
				}
				if(!added) {
					//first time, need to create a new path
					VirtualPath newPath = new VirtualPath();
					newPath.addNode(node);
					pathList.add(newPath);
				}

				//create the resource from the subject in order to obtain the surrounding triples
				Resource subj = new URIImpl(node.getIri());
				//take all the surrounding triples
				Model star = graph.filter(subj, null, null);

				//for each neighbor triple
				for(Statement t : star) {

					//check the object 
					String object = t.getObject().toString();
					//if already visited (loop)
					if(alreadyVisitedIRI.contains(object)) {
						node.addTriple(t);
						continue;
					}

					//					System.out.println(t);
					counter++;

					if(!UrlUtilities.checkIfValidURL(object)) {
						//it is a literal. Add the triple to this VNode
						node.addTriple(t);
					} else {
						//the object is a iri

						//create a new node for it
						VNode objNode = new VNode();
						objNode.setIri(object);
						objNode.setPrevious(node);

						//add this object to the nodes which are next from this source
						node.addNxt(objNode);

						//add it to the discovered nodes
						nodeQueue.add(objNode);
					}
				}//end for each neighbor
			}//end of the exploration

			//cleanup
			System.out.println("before " + pathList.size());
			Iterator<VirtualPath> iterator = pathList.iterator();
			while(iterator.hasNext()) {
				VirtualPath path = iterator.next();
				if(path.isToDelete()) {
					iterator.remove();
				}
			}

			System.out.println("after " + pathList.size());
			System.out.println(startNode);

			iterator = pathList.iterator();
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
			while(iterator.hasNext()) {
				VirtualPath path = iterator.next();
				path.pruneWithQueryWords(queryWords);
			}




		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	/**@param root the docno of the root document
	 * */
	private void scoreOneRoot2(String root) {
		//take the path of the graph using the root as key
		String graphPath = this.pathMap.get(root);

		try {
			//read the graph
			Model graph = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graphPath));
			//open the connection to RDB
			Connection connection = 
					ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());

			//get the iri from the docno which is the root saved in database
			PreparedStatement pst = connection.prepareStatement(SQL_FIND_NODE_IRI);
			pst.setInt(1, Integer.parseInt(root));
			ResultSet rs = pst.executeQuery();
			rs.next();//pretty sure it has a next, hope I don't need to change the code
			String sbjIri = rs.getString("node_name");

			//sbjIri is the IRI of the root node of this virtual document
			//now we prepare a BFS

			//queue for the discovered nodes
			Queue<VNode> nodeQueue = new LinkedList<VNode>();
			//list for the IRI already discovered
			List<String> alreadyVisitedIRI = new ArrayList<String>();


			//root (starting) node
			VNode rootNode = new VNode();
			//set the iri of the root and add to the queue and to the already discovered nodes
			rootNode.setIri(sbjIri);
			rootNode.setId(Integer.parseInt(root));
			nodeQueue.add(rootNode);
			alreadyVisitedIRI.add(sbjIri);
			int counter = 0;

			//while for the BFS
			while(! nodeQueue.isEmpty() ) {
				//take the first node
				VNode s = nodeQueue.poll();

				//now find the neighbors of the node
				Resource subject = new URIImpl(s.getIri());
				Model outStar = graph.filter(subject, null, null);

				//now outStar contains all the triples with s as subject (if any)
				for(Statement t : outStar) {
					//take the object and check if it is a literal or an IRI
					String object = t.getObject().toString();
					if(!alreadyVisitedIRI.contains(object)) {
						//a new object


						if(UrlUtilities.checkIfValidURL(object)) {

							//sign that it has been discovered or we are all in trouble
							alreadyVisitedIRI.add(object);

							//if it is a IRI, we need to create a new node
							VNode objectNode = new VNode();
							objectNode.setIri(object);
							objectNode.setPrevious(s);
							objectNode.setPreviousStatement(t);

							//set this node as a neighbour for s
							s.addNxt(t, objectNode);

							nodeQueue.add(objectNode);
							counter++;
						} else {
							//this is simply a literal object, and we add it in the accessory cloud of s
							s.addTriple(t);
							counter++;
						}
					} else {
						/*this object node has already been explored
						 * the triple can tell something new thanks to the predicate,
						 * so for now I add it in the accessory cloud*/
						s.addTriple(t);
						counter++;
					}
				}
			}//end while

			//now we found ourselves with a chain of nodes. We can prune them using the query words
			System.setProperty("tokeniser", "EnglishTokeniser");
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
			System.setProperty("tokeniser", "BigramTokeniser");
			List<String> queryWordsBigrams = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
			System.setProperty("tokeniser", "EnglishTokeniser");
			this.pruneFromRootWithQueryWords(rootNode, queryWords);

			//now we have all nice and pruned, so we proceed to create the graph and score it
			try {
				this.buildAndScoreTheGraph(rootNode, queryWords, queryWordsBigrams);
			} catch (Exception e) {
				e.printStackTrace();
			}

		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/** pruning heuristic starting from the root
	 * */
	private void pruneFromRootWithQueryWords(VNode root, List<String> queryWords) {
		Queue<VNode> executingQueue = new LinkedList<VNode>();
		executingQueue.add(root);
		List<VNode> leaves = new ArrayList<VNode>();
		int counter = 0;

		while(! executingQueue.isEmpty() ) {
			//take the node
			VNode s = executingQueue.poll();
			//first of all, look at all the accessory triples around the root
			List<Statement> cloud = s.getNodeCloud();
			//get an iterator over the cloud
			Iterator<Statement> iterator = cloud.iterator();

			//prune the cloud
			while(iterator.hasNext()) {
				//get the statement
				Statement t = iterator.next();
				/*check only predicate and object, otherwise it is useless
				 * all triples will be maintained*/
				String pred = t.getPredicate().toString();
				String obj = t.getObject().stringValue();
				String tripleDoc = "";
				tripleDoc = UrlUtilities.takeWordsFromIri(pred);
				if(UrlUtilities.checkIfValidURL(obj)) {
					tripleDoc += " " + UrlUtilities.takeWordsFromIri(obj);
				} else {
					tripleDoc += " " + obj;
				}
				List<String> tripleWords = 
						TerrierUsefulMethods.
						getDocumentWordsWithTerrierAsList(tripleDoc);
				tripleWords.retainAll(queryWords);
				if(tripleWords.size() == 0){
					//need to delete the triple
					iterator.remove();
					counter++;
				}

			}//end pruning the cloud phase

//			System.out.println("removal perfomed " + counter);

			//now look at the neighbors
			List<Pair<Statement, VNode>> neighborsList = s.getNxtsList();
			for(Pair<Statement, VNode> pair : neighborsList) {
				VNode u = pair.getRight();
				executingQueue.add(u);
			}

			if(neighborsList.size() == 0) {
				leaves.add(s);
			}

		}//end of the first direct traversal
		//DEBUG
//		System.out.println("number of leaves :" + leaves.size() + "\nremoval perfomed " + counter);

		//now pass the ball to the pruning from the leaves
		this.pruneFromTheLeavesWithQueryWords(leaves, queryWords);
	}

	/** Here we prune from the leaves going back to the root */
	private void pruneFromTheLeavesWithQueryWords(List<VNode> leaves, List<String> queryWords) {
		//first of all, create a queue with the nodes we are visiting starting from the leaves
		Queue<VNode> executingQueue = new LinkedList<VNode>(leaves);
		int count = 0;
		while(!executingQueue.isEmpty()) {
			VNode s = executingQueue.poll();
			if(s.isToSpare())
				continue;

			//check if this node and its surroundings deserved to be maintained or not

			//string representing the document connected to this node
			String nodeDoc = "";

			/*tecnically if I am here with some literal
			 * neighbours the node should be maintained because this nodes keep
			 * some query words inside of them. But better be safe*/

			//take the accessory cloud of this node
			List<Statement> cloud = s.getNodeCloud();
			for(Statement t : cloud) {
				//take the text from the surrounding triples
				String pred = t.getPredicate().toString();
				String obj = t.getObject().stringValue();
				nodeDoc += " " + UrlUtilities.takeWordsFromIri(pred);
				if(UrlUtilities.checkIfValidURL(obj)) {
					nodeDoc += " " + UrlUtilities.takeWordsFromIri(obj);
				} else {
					nodeDoc += " " + obj;
				}
			}//end for on the cloud

			//add the contribution of this node
			String iri = s.getIri();
			nodeDoc += " " + UrlUtilities.takeWordsFromIri(iri);

			//add the contribution from the connecting triple if present
			Statement t = s.getPreviousStatement();
			String pred = "";
			if(t!=null) {//for the root
				pred = t.getPredicate().toString();
				nodeDoc += " " + UrlUtilities.takeWordsFromIri(pred);
			}

			//now convert the document in a list of strings
			List<String> nodeWords = 
					TerrierUsefulMethods.
					getDocumentWordsWithTerrierAsList(nodeDoc);
			nodeWords.retainAll(queryWords);

			//now, if the list is empty, remove this node from the path
			if(nodeWords.size() == 0) {
				VNode parent = s.getPrevious();

				if(parent==null)
					continue;

				List<Pair<Statement, VNode>> list = parent.getNxtsList();
				Iterator<Pair<Statement, VNode>> iterator =  list.iterator();
				while(iterator.hasNext()) {
					Pair<Statement, VNode> pair = iterator.next();
					Statement st = pair.getLeft();
					VNode neighbor = pair.getRight();
					if(st.getPredicate().toString().equals(pred) &&
							neighbor.isEqualByIri(s)) {
						iterator.remove();
						break;//there is only one!
					}
				} //end of remotion

				//now, we need to add the parent to the list of nodes to be checked
				if(!parent.isToSpare())
					executingQueue.add(parent);
			} else {
				/*we need to signal to the parent that one of its neighbor
				 * is ok, in order to avoid to break paths that don't 
				 * need to be broken*/
				VNode parent = s.getPrevious();
				if(parent!=null)//it could be the root
					parent.setToSpare(true);
			}

		}
		//now the pruning is complete
	}

	/** computes the contribution of a word to the wtf of that 
	 * word in this document in this position
	 * */
	private double computeGaussianWeight(int frequency, 
			double rootStaticScore, 
			double staticWeight) {
		double score = 0;
		//argument of the exponent
		double arg = (staticWeight - rootStaticScore);
		arg = Math.pow(arg, 2);
		arg = -arg;
		arg = (double) arg / 2;
		double exp = Math.exp(arg);
		score = exp * frequency;

		return score;
	}

	/** Builds, prints and scores the graph 
	 * @throws Exception */
	private void buildAndScoreTheGraph(VNode root, List<String> queryWords,
			List<String> queryWordsBigram) throws Exception {
		Model graph = new TreeModel();
		Queue<VNode> nodeQueue = new LinkedList<VNode>();
		nodeQueue.add(root);

		//map with the wtf for every word inside this document
		Map<String, Double> unigramMap = new HashMap<String, Double>();
		Map<String, Double> bigramMap = new HashMap<String, Double>();
		double rootStaticScore = 0;

		//begin BFS
		while( !nodeQueue.isEmpty() ) {
			VNode s = nodeQueue.poll();
			//get the triples of the cloud
			List<Statement> cloud = s.getNodeCloud();
			//add them to the graph
			graph.addAll(cloud);
			//now take all the neighbors
			List<Pair<Statement, VNode>> neighborsList = s.getNxtsList();

			//now for the static score of the path
			//get the static weight of the path so far
			double staticWeight = s.getStaticPathWeight();
			if(staticWeight == 0) {
				//special case, the root
				staticWeight = (double) 1 / (Math.log(Math.E + neighborsList.size()));
				rootStaticScore = staticWeight;
			} else {
				//add to the static weight computed so far the contribution of the node itself
				staticWeight += (double) 1 / (Math.log(Math.E + neighborsList.size()));
			}
			s.setStaticPathWeight(staticWeight);

			///////
			//update the tables with the information about the weighted frequencies of the words
			this.updateWordsContribution(s, unigramMap, bigramMap, rootStaticScore, staticWeight);

			Iterator<Pair<Statement, VNode>> iter = neighborsList.iterator();
			while(iter.hasNext()) {
				Pair<Statement, VNode> pair = iter.next();
				//get the statement and add it to the graph
				Statement t = pair.getLeft();
				graph.add(t);
				//get the neighbor and add it to the queue
				VNode u = pair.getRight();
				nodeQueue.add(u);
				//set the weight of the path until s, then u will complete the score with itself
				u.setStaticPathWeight(staticWeight);
			}
		}//end of the construction of the graph
		//now we can score and print the graph
		
		//compute the length for unigrams and bigrams
		Pair<Double, Double> lengths = this.computeTheLengths(unigramMap, bigramMap);
		
		double finalScore = this.scoreThisGraph(unigramMap, bigramMap, queryWords, queryWordsBigram, lengths);
		YosiDocument document = new YosiDocument();
		document.setDocno(root.getId()+"");
		document.setScore(finalScore);
		
		docList.add(document);

		//now print the graph and go in peace
		String outputPath = this.graphsOutputDirectory + "/" + root.getId() + ".ttl";
		BlazegraphUsefulMethods.printTheDamnGraph(graph, outputPath);

	}
	
	/** Compute the lengths of the current document*/
	private Pair<Double, Double> computeTheLengths(Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap) {
		double uLength = 0;
		for(Entry<String, Double> e : unigramMap.entrySet()) {
			uLength += e.getValue();
		}
		
		double bLength = 0;
		for(Entry<String, Double> e : bigramMap.entrySet()) {
			bLength += e.getValue();
		}
		
		return new MutablePair<Double, Double>(uLength, bLength);
	}

	/** Updates the maps about the wtf of the words in this document
	 * @throws Exception */
	private void updateWordsContribution(VNode s, 
			Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap,
			double rootStaticScore,
			double staticWeight) throws Exception {
		//now we work on the words of this node
		String nodeText = s.getNodeText();
		//work with terrier - take all the words in this document, index this node document
		MemoryIndex nodeIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(nodeText);
		System.setProperty("tokeniser", "EnglishTokeniser");
		List<String> documentWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(nodeText);
		
		//NB: getDirectIndex not implemented, returns null
//		PostingIndex<Pointer> di = (PostingIndex<Pointer>) nodeIndex.getDirectIndex();
//		DocumentIndex doi = nodeIndex.getDocumentIndex();
		Lexicon<String> lex = nodeIndex.getLexicon();
		//list to keep track of the already visited words (do you know Terrier
		//has not implemented for the getDirectIndex for the MemoryIndex? Terrific.)
		Set<String> alreadyCheckedWords = new HashSet<String>();
		for(String word : documentWords) {
			if(!alreadyCheckedWords.contains(word)) {
				alreadyCheckedWords.add(word);
				if(word!=null) {
					LexiconEntry lee = lex.getLexiconEntry(word);
					if(lee==null)
						continue;
					int freq = lee.getFrequency();
					double score = this.computeGaussianWeight(freq, rootStaticScore, staticWeight);
					Double oldScore = unigramMap.get(word);
					if(oldScore == null) {
						unigramMap.put(word, score);
					} else {
						unigramMap.put(word, oldScore + score); 
					}
				}
			}
		}
		
		nodeIndex.close();

		//do the same for the bigrams
		nodeIndex = TerrierUsefulMethods.getBigramMemoryIndexFromDocument(nodeText);
		System.setProperty("tokeniser", "BigramTokeniser");
		documentWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(nodeText);
		lex = nodeIndex.getLexicon();
		
		for(String word : documentWords) {
			if(!alreadyCheckedWords.contains(word)) {
				alreadyCheckedWords.add(word);
				if(word!=null) {
					LexiconEntry lee = lex.getLexiconEntry(word);
					if(lee==null)
						continue;
					int freq = lee.getFrequency();
					double score = this.computeGaussianWeight(freq, rootStaticScore, staticWeight);
					Double oldScore = bigramMap.get(word);
					if(oldScore == null) {
						bigramMap.put(word, score);
					} else {
						bigramMap.put(word, oldScore + score); 
					}
				}
			}
		}
		
		System.setProperty("tokeniser", "EnglishTokeniser");
		
		nodeIndex.close();
		//completed the update of the scores
	}


	/** Executes the scoring of the answer graph 
	 * <p>
	 * NB: in the dirichlet smoothing,
	 * I am using the virtual collection as support, both 
	 * considering V and both when computing alpha.
	 * Maybe to improve performances I will try to
	 * firstly print all the answer graphs and then do the
	 * computation with the answer collection.
	 * @throws IOException 
	 * */
	private double scoreThisGraph(Map<String, Double> unigramMap, Map<String, Double> bigramMap, 
			List<String> queryWords,
			List<String> queryWordsBigram,
			Pair<Double, Double> lengths) throws IOException {
		
		double score = 0;
		Pair<Double, Double> alphas = computeAlphas(this.currentRootId);
		//sum over all the query words
		for(String queryWord : queryWords) {
			score += this.scoreOneQueryWordUnigram(unigramMap, bigramMap, queryWord, alphas, lengths);
		}
		
		for(String queryWord : queryWordsBigram) {
			score += this.scoreOneQueryWordBigram(bigramMap, queryWord, alphas.getRight(), lengths.getRight());
		}
		
		return score;
	}

	private Pair<Double, Double> computeAlphas(int rootDocid) throws IOException {
		//first alpha for unigrams
		double lambda = this.virtualIndex.getCollectionStatistics().getAverageDocumentLength();
		//get |x|, the length of this document
		PostingIndex<Pointer> di = (PostingIndex<Pointer>) this.virtualIndex.getDirectIndex();
		DocumentIndex doi = virtualIndex.getDocumentIndex();
		IterablePosting postings = di.getPostings(doi.getDocumentEntry(rootDocid));
		int x = postings.getDocumentLength();
		double alpha_u = (double) lambda / (lambda + x);

		//first alpha for unigrams
		lambda = this.virtualIndexBigrams.getCollectionStatistics().getAverageDocumentLength();
		//get |x|, the length of this document
		di = (PostingIndex<Pointer>) this.virtualIndexBigrams.getDirectIndex();
		doi = virtualIndex.getDocumentIndex();
		postings = di.getPostings(doi.getDocumentEntry(rootDocid));
		x = postings.getDocumentLength();
		double alpha_b = (double) lambda / (lambda + x);
		
		Pair<Double, Double> alphas = new MutablePair<Double, Double>(alpha_u, alpha_b);
		return alphas;
	}

	private double scoreOneQueryWordUnigram(Map<String, Double> unigramMap, 
			Map<String, Double> bigramMap, 
			String queryWord,
			Pair<Double, Double> alphas,
			Pair<Double, Double> lengths) {

		//***** UNIGRAM *****
		double alpha_u = alphas.getLeft();
		//get the wtf for this word
		Double wtf_q_v = unigramMap.get(queryWord);
		if(wtf_q_v == null)
			wtf_q_v = 0.0;
		double uLength = lengths.getLeft();
		double p1 = (double) wtf_q_v / uLength;
		
		Lexicon<String> lex = this.virtualIndex.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(queryWord);
		int num = 0;
		if(le!=null) {
			num = le.getFrequency();
		}
		double den = this.virtualIndex.getCollectionStatistics().getNumberOfTokens();
		double p2 =(double) num / den;
		
		double logArgument = (1 - alpha_u) * p1 + (alpha_u) * p2;
		if(logArgument==0)
			return 0;
		return Math.log(logArgument);

	}
	
	private double scoreOneQueryWordBigram(Map<String, Double> bigramMap,
			String queryWordBigram,
			double alpha,
			double length) {
		
		//*** BIGRAM *****
		//get the wtf for this query word
		Double wtf_q_v = 0.0;
		if(bigramMap.get(queryWordBigram) != null)
			wtf_q_v = bigramMap.get(queryWordBigram);
		double p1 = (double) wtf_q_v / length;
		
		Lexicon<String> lex = this.virtualIndexBigrams.getLexicon();
		LexiconEntry le = lex.getLexiconEntry(queryWordBigram);
		int num = 0;
		if(le!=null) {
			num = le.getFrequency();
		}
		double den = this.virtualIndexBigrams.getCollectionStatistics().getNumberOfTokens();
		double p2 =(double) num / den;
		
		double logArgument = (1 - alpha) * p1 + (alpha) * p2;
		if(logArgument==0)
			return 0;//otherwise we have -Infinity, so eliminate this contribution
		return Math.log(logArgument);
		
	}

	/** Returns a list of docno/docid (now docid)
	 * of the nodes that are roots of documents with all the query words
	 * inside of them.
	 * 
	 * */
	public List<String> findBestRoots(Model graph) {
		List<String> alreadyCheckedSubjects = new ArrayList<String>();

		try {
			//open the connection
			Connection connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());
			//open the index
			Index index = IndexOnDisk.createIndex(this.virtualIndexPath, "data");
			this.virtualIndex = index;
			Index bigramIndex = IndexOnDisk.createIndex(this.virtualIndexPathBigram, "data");
			this.virtualIndexBigrams = bigramIndex;

			PostingIndex<Pointer> di = (PostingIndex<Pointer>) index.getDirectIndex();
			DocumentIndex doi = index.getDocumentIndex();
			Lexicon<String> lex = index.getLexicon();

			System.setProperty("tokeniser", "EnglishTokeniser");
			List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);

			//now get all the virtual documents that contain all the query words
			List<String> documentsDocno = getDocumentWithAllTheQueryWords(index, queryWords);

			int distinctCounter = 0;

			//list containign all the words of the document
			List<String> workingDocuments = new ArrayList<String>();

			//iterate through the graph
			/*now we look at all the subjects in the query graph and take
			 * their corresponding virtual documents. If these documents are contained
			 * in the candidates of the documentsDocno list, we add them to
			 * the workingDocuments list. These nodes are the roots 
			 * we will use later as starting point*/
			for(Statement t : graph) {
				//get the subject
				String subj = t.getSubject().stringValue();
				//in case we already did this node, go on
				if(alreadyCheckedSubjects.contains(subj))
					continue;

				//update the list of already checked subjects
				alreadyCheckedSubjects.add(subj);


				PreparedStatement st = connection.prepareStatement(SQL_FIND_NODE);
				st.setString(1, subj);
				ResultSet rs = st.executeQuery();
				if(rs.next()) {
					//get the id of the graph and the corresponding terrier id
					int docno = rs.getInt("id_");
					int docid = rs.getInt("terrier_id");
					distinctCounter++;

					//now we check if this docno is present among the docnos of the virtual documents
					String sDocno = ""+docno;
					String sDocid = ""+docid;

					if(documentsDocno.contains(sDocid)) {
						workingDocuments.add(sDocid);
						System.out.print(sDocno + ", ");
					}

				}
			}
			ConnectionHandler.closeConnectionIfOwner(this.getClass().getName());

			return workingDocuments;

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	/** Given an index and a list of query words,
	 * returns the list of docno/docid (now docid) of the documents that contain 
	 * all thw words.
	 * */
	private List<String> getDocumentWithAllTheQueryWords(Index index, List<String> queryWords) throws IOException {
		//get the inverted and meta indexes
		PostingIndex<Pointer> invertedIndex = (PostingIndex<Pointer>) index.getInvertedIndex();
		MetaIndex meta = index.getMetaIndex();

		//use the index to know which documents have the query words
		Lexicon<String> lexicon = index.getLexicon();
		List<String> candidatesList = new ArrayList<String>();

		//for the first query word
		LexiconEntry le1 = lexicon.getLexiconEntry(queryWords.get(0));
		if(le1!=null) {
			//get the documents containing the query word
			IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le1);
			while (postings.next() != IterablePosting.EOL) {
				int docid = postings.getId();
				String docno = meta.getItem("docno", docid);
				//				candidatesList.add(docno);XXX
				candidatesList.add(""+docid);
			}
		}

		//now proceed with the other keywords. Do the intersections of the sets containing the words
		//to get at the end only the documents with all the keywords
		for(int i = 1; i< queryWords.size(); ++i) {
			//find the documents of this query word
			List<String> newSet = new ArrayList<String>();
			LexiconEntry le = lexicon.getLexiconEntry(queryWords.get(i));
			if(le != null) {
				IterablePosting postings = invertedIndex.getPostings((BitIndexPointer) le);
				while (postings.next() != IterablePosting.EOL) {
					int docid = postings.getId();
					String docno = meta.getItem("docno", docid);
					//					newSet.add(docno);XXX
					newSet.add(docid+"");
				}
				//intersection of the new list with the first one
				candidatesList.retainAll(newSet);
			}
		}
		return candidatesList;
	}

	/** Method to be called at the beginning. 
	 * There was a little frustrating thing: the id of terrier
	 * cannot be directly connected to the docno of the documents. 
	 * Need to insert it manually in the database*/
	private void setupTheSystem() {
		try {
			Connection connection = ConnectionHandler.createConnectionAsOwner(jdbcConnectingString, this.getClass().getName());

			//open the index
			//open the new index
			Index index = IndexOnDisk.createIndex(this.virtualIndexPath, "data");
			MetaIndex meta = index.getMetaIndex();
			int nod = index.getCollectionStatistics().getNumberOfDocuments();
			for (int i = 0; i<nod; ++i) {
				//i here is the terrer docid

				//get the docno of the document
				String docno = meta.getItem("docno", i);
				//insert into the database
				String sql_update = "UPDATE node set terrier_id=? where id_=?";
				PreparedStatement stmt = connection.prepareStatement(sql_update);
				stmt.setInt(1, i);
				stmt.setInt(2, Integer.parseInt(docno));
				stmt.executeUpdate();

			}

		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** creates a mao that is the correspondence of graphs ids and their paths*/
	private Map<String, String> getMapOfPaths() {
		Map<String, String> pathMap = new HashMap<String, String>();

		File f = new File(this.virtualGraphsMainDirectory);
		File[] files = f.listFiles();
		for(File dir : files) {
			if(!dir.getName().equals(".DS_Store")) {
				File[] graphs = dir.listFiles();
				for(File graph : graphs) {
					if(graph.getName().equals(".DS_Store"))
						continue;
					//for every file inside a directory

					//get the docno of the graph
					String graphName = graph.getName();
					String[] parts = graphName.split("\\.");
					graphName = parts[0];
					String absPath = graph.getAbsolutePath();
					pathMap.put(graphName, absPath);
				}
			}
		}
		return pathMap;
	}


	/** Test main*
	 */
	public static void main(String[] args) {
		SmartExplorationPhase execution = new SmartExplorationPhase();
//		execution.setupTheSystem();
		execution.smartTraversing();
	}

	public String getRunFile() {
		return runFile;
	}

	public void setRunFile(String runFile) {
		this.runFile = runFile;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getAnswerGraphsDirectory() {
		return answerGraphsDirectory;
	}

	public void setAnswerGraphsDirectory(String answerGraphsDirectory) {
		this.answerGraphsDirectory = answerGraphsDirectory;
	}

	public String getBlazegraphFilePath() {
		return blazegraphFilePath;
	}

	public void setBlazegraphFilePath(String blazegraphFilePath) {
		this.blazegraphFilePath = blazegraphFilePath;
	}

	public String getJdbcConnectingString() {
		return jdbcConnectingString;
	}

	public void setJdbcConnectingString(String jdbcConnectingString) {
		this.jdbcConnectingString = jdbcConnectingString;
	}

	public String getVirtualIndexPath() {
		return virtualIndexPath;
	}

	public void setVirtualIndexPath(String virtualIndexPath) {
		this.virtualIndexPath = virtualIndexPath;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getResultOutputFile() {
		return resultOutputFile;
	}

	public void setResultOutputFile(String resultOutputFile) {
		this.resultOutputFile = resultOutputFile;
	}

	public String getVirtualGraphsMainDirectory() {
		return virtualGraphsMainDirectory;
	}

	public void setVirtualGraphsMainDirectory(String virtualGraphsMainDirectory) {
		this.virtualGraphsMainDirectory = virtualGraphsMainDirectory;
	}

	public String getGraphsOutputDirectory() {
		return graphsOutputDirectory;
	}

	public void setGraphsOutputDirectory(String graphsOutputDirectory) {
		this.graphsOutputDirectory = graphsOutputDirectory;
	}

	public int getQueryId() {
		return queryId;
	}

	public void setQueryId(int queryId) {
		this.queryId = queryId;
	}
}
