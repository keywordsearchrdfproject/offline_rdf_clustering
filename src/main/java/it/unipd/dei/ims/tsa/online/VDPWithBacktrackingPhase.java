package it.unipd.dei.ims.tsa.online;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.Set;
import java.util.logging.LogManager;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.openrdf.model.Model;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.LinkedHashModel;
import org.openrdf.model.impl.TreeModel;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.terrier.realtime.memory.MemoryIndex;
import org.terrier.structures.Lexicon;
import org.terrier.structures.LexiconEntry;

import it.unipd.dei.ims.datastructure.ConnectionHandler;
import it.unipd.dei.ims.datastructure.DatabaseState;
import it.unipd.dei.ims.datastructure.ThreadState;
import it.unipd.dei.ims.rum.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.summ.datastructures.BackwardQueues;
import it.unipd.dei.ims.summ.datastructures.BackwardT;
import it.unipd.dei.ims.summ.datastructures.BackwardTList;
import it.unipd.dei.ims.summ.datastructures.ExtendedBackwardTList;
import it.unipd.dei.ims.summ.datastructures.KeywordNode;
import it.unipd.dei.ims.summ.datastructures.SummDocument;
import it.unipd.dei.ims.summ.datastructures.SummDocumentComparator;
import it.unipd.dei.ims.summ.datastructures.SummNode;
import it.unipd.dei.ims.summ.online.Summ;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.tsa.datastructure.VNode;

public class VDPWithBacktrackingPhase extends VirtualDocAndPruningOnlinePhase {

	/** A list with the processed query words 
	 * */
	protected List<String> queryWords;

	/** Map here in the Backward algorithm. The key here
	 * is the url of the root node instead of its id,
	 * as can be found in {@link Summ}. */
	private Map<String, ExtendedBackwardTList> M;


	/** value of maximum number of answers we retrieve from one subgraph
	 * (even different roots).
	 * This value was added taking into consideration the number of potential answers
	 * produced by the algorithm in order to differentiate them.
	 * The default is 1.
	 * */
	private int tooManyAnswersForThisSubgraph;

	/** List containing the answers graph found in this execution of backward 
	 * */
	List<SummDocument> answerList;

	/** maximum number of answers accepted for the algorithm.
	 * This is the value k. Default is 100.
	 *  */
	private int tooManyAnswersForThisAlgorithm;

	private String answerDirectory;

	/**Maximum number of answers accepted for one single root*/
	private int tooManyAnswersForOneRoot;

	private int answersFromOneGraphCounter;

	/** this string contains the name of the method that will be printed
	 * in the resulting trec files. Originally,
	 * for this class, it was meant to be TSA+VDP+BACK (A)
	 * or TSA+BACK (B)
	 * 
	 * 
	 * */
	private String method;




	public String getAnswerDirectory() {
		return answerDirectory;
	}

	public void setAnswerDirectory(String answerDirectory) {
		this.answerDirectory = answerDirectory;
	}

	public VDPWithBacktrackingPhase() {
		super();

		answersFromOneGraphCounter = 0;

		
		tooManyAnswersForThisAlgorithm = 100;
		tooManyAnswersForThisAlgorithm = Integer.parseInt(this.map.get("too.many.answers.for.this.algorithm"));
		tooManyAnswersForThisSubgraph = Integer.parseInt(this.map.get("too.many.answers.for.this.subgraph"));
		tooManyAnswersForOneRoot = Integer.parseInt(this.map.get("too.many.answers.for.one.root"));


		M = new HashMap<String, ExtendedBackwardTList>();
		answerList = new ArrayList<SummDocument>();
	}

	public int getTooManyAnswersForThisAlgorithm() {
		return tooManyAnswersForThisAlgorithm;
	}

	public void setTooManyAnswersForThisAlgorithm(int tooManyAnswersForThisAlgorithm) {
		this.tooManyAnswersForThisAlgorithm = tooManyAnswersForThisAlgorithm;
	}



	/** One of the two main methods of this class (method A). 
	 * Implements the TSA+VDP+backward solution.
	 * 
	 * First, reads the top n graph produced by the TSA+BM25,
	 * producing the query graph. Then it looks for subgraphs inside this query graph that contain
	 * all the keywords.
	 * <p>
	 * These subgraphs are looked starting from nodes randomly inside the graph. Then, a BFS is performed 
	 * starting from these nodes, keeping ourselves inside a certain radius tau. 
	 * Then, we check for the words inside this graph. if it contains all the keywords, it
	 * is kept and a work starts on it.
	 * <p>
	 * The graph is first pruned from the outside going inside, where the 'inside' is defined by the node 
	 * which was used as root node by the BFS algorithm.
	 * Until now, the algorithm is equal to the base TSA+VDP one.
	 * <p>
	 * Now on the pruned graph the backward algorithm is executed. THis should return at 
	 * least one subtree. The implementation makes sure that exactly one tree is retained
	 * from every graph found so far.
	 * <p>
	 * Every subgraph, before the pruning, is also removed from the query graph.
	 * <p>
	 * Another technicality is that the query graph is simply a base to find starting nodes.
	 * The creation of subgraph is performed on the whole graph. This was seen to be a good heuristic
	 * to obtain the higher values of tb-DCG. In the future it could be interesting
	 * to test the creation of subgraphs only on the query graph.
	 * */
	public void tsa_vdp_backward() {

		LogManager.getLogManager().reset();

		File f = new File(this.answerGraphOutputDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}
		try {
			FileUtils.cleanDirectory(f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}




		Model queryGraph = null;

		//get the words of the query right
		System.setProperty("tokeniser", "EnglishTokeniser");
		this.queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);

		if(DatabaseState.getSize() == DatabaseState.SMALL) {
			//better for smaller graphs
			System.out.println("Settings for a small dataset in VDP");
			queryGraph = this.readQueryGraphNoFilterVersion();			
		}
		if(DatabaseState.getSize() == DatabaseState.BIG) {
			//better for bigger graphs
			System.out.println("Settings for a big dataset in VDP");
			try {
				queryGraph = this.readQueryGraph();							
			} catch (java.nio.file.NoSuchFileException e) {
				System.err.println("no files to read");
				return;
			}
		}

		//*with batman voice* Answer me!!!
		//looks for the subgraphs inside the query graph containing all the keywords
		try {
			this.lookForAnswers(queryGraph);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public void lookForAnswers(Model graph) throws SQLException {
		connection = ConnectionHandler.createConnectionAsOwner(this.jdbcConnectionString, this.getClass().getName());

		//set to keep track of the subjects already checked
		Set<String> alreadyExploredSubjects = new HashSet<String>();

		int progressCounter = 0;

		for(Statement t : graph) {
			//for every statement in the query graph, take the subject
			//and check if it has never been explored

			if(Thread.interrupted() || !ThreadState.isOnLine()) {
				ThreadState.setOnLine(false);
				break;
			}

			String subject = t.getSubject().toString();
			if(alreadyExploredSubjects.contains(subject))
				continue;
			alreadyExploredSubjects.add(subject);

			Model subgraph = this.exploreOneRoot(subject, graph, alreadyExploredSubjects);
			if(subgraph == null)
				continue; //nothing to see here

			//if we are here, the graph contains all the keywords. We use it.
			this.processOneGraph(subject, subgraph);

		}

		//if we are here, we can print our final results
		//if we are here it means that the algorithm finished or the time went out
		//in any case, we write our answers
		Collections.sort(this.answerList, new SummDocumentComparator());
		this.printResults();
	}

	private void processOneGraph(String subject_, Model subgraph) {

		VNode rootNode = this.buildOneGraphFromOneRoot(subject_, subgraph);

		//now we have one graph, with root in s, which has been signed
		//with our data structures and works as a sort of tree
		//we can prune it now
		this.pruneFromRootWithQueryWords(rootNode, queryWords);
		Model prunedGraph = this.buildFromTheRoot(rootNode);

		//now that we have the pruned graph, we can perform backward on it
		this.backtrackInOne(prunedGraph);
	}

	/**	rebuild the graph with BFS from the root
		here I am using an object that I called VNode
		the VNode has fields that enable us to re-create chains from the
		source to the rest of the graph.*/
	protected VNode buildOneGraphFromOneRoot(String subject_, Model subgraph) {
		//queue for the discovered nodes
		Queue<VNode> nodeQueue = new LinkedList<VNode>();
		List<String> alreadyVisitedIRI = new ArrayList<String>();

		//root (starting) node
		VNode rootNode = new VNode();
		rootNode.setIri(subject_);
		nodeQueue.add(rootNode);
		alreadyVisitedIRI.add(subject_);

		while(! nodeQueue.isEmpty() ) {
			VNode s = nodeQueue.poll();
			//now find the neighbors of the node
			Resource subject = new URIImpl(s.getIri());
			Model outStar = subgraph.filter(subject, null, null);

			//now outStar contains all the triples with s as subject (if any)
			for(Statement t : outStar) {
				//take the object and check if it is a literal or an IRI
				String object = t.getObject().toString();
				if(!alreadyVisitedIRI.contains(object)) {
					//a new object
					if(UrlUtilities.checkIfValidURL(object)) {

						//sign that it has been discovered or we are all in trouble
						alreadyVisitedIRI.add(object);

						//if it is a IRI, we need to create a new node
						VNode objectNode = new VNode();
						objectNode.setIri(object);
						objectNode.setPrevious(s);
						objectNode.setPreviousStatement(t);

						//set this node as a neighbour for s
						s.addNxt(t, objectNode);

						nodeQueue.add(objectNode);
					} else {
						//this is simply a literal object, and we add it in the accessory cloud of s
						s.addTriple(t);
					}
				} else {
					/*this object node has already been explored
					 * the triple can tell something new thanks to the predicate,
					 * so for now I add it in the accessory cloud*/
					s.addTriple(t);
				}
			}
		}//end while

		return rootNode;

	}

	protected Model buildFromTheRoot(VNode root) {
		Model graph = new TreeModel();
		Queue<VNode> nodeQueue = new LinkedList<VNode>();
		nodeQueue.add(root);

		//begin BFS
		while( !nodeQueue.isEmpty() ) {
			VNode s = nodeQueue.poll();
			//get the triples of the cloud
			List<Statement> cloud = s.getNodeCloud();
			//add them to the graph
			graph.addAll(cloud);
			//now take all the neighbors
			List<Pair<Statement, VNode>> neighborsList = s.getNxtsList();

			Iterator<Pair<Statement, VNode>> iter = neighborsList.iterator();
			while(iter.hasNext()) {
				Pair<Statement, VNode> pair = iter.next();
				//get the statement and add it to the graph
				Statement t = pair.getLeft();
				graph.add(t);
				//get the neighbor and add it to the queue
				VNode u = pair.getRight();
				nodeQueue.add(u);
			}
		}//end of the construction of the graph
		return graph;
	}

	/** Given one root, explores the whole database (not only the query graph)
	 * starting from that root. Then, if this subgraph contains all the query words,
	 * returns and deletes the nodes explored with this exploration from the query graph.
	 * <p>
	 * You may ask yourself why we are exploring the whole graph and not 
	 * only the query graph. This is due to the fact that I saw a little
	 * bit more of tb-DCG with this solution. Got to 
	 * crunck up those numbers, you know. In the future
	 * we may need to use an implementation over the only query graph.
	 * */
	protected Model exploreOneRoot(String subject, Model queryGrph, Set<String> exploredSubjects) throws SQLException {
		Model subgraph = new TreeModel();
		//perform a BFS algorithm
		VNode source = new VNode();
		source.setIri(subject);
		source.setRadius(0);

		Set<String> nodesVisitedNow = new HashSet<String>();

		//a queue that contains IRIs to visit
		Queue<VNode> iris = new LinkedList<VNode>();
		iris.add(source);

		while(!iris.isEmpty()) {
			VNode v = iris.remove();
			String vIri = v.getIri();
			int vRadius = v.getRadius();

			//add the string to the visited nodes
			nodesVisitedNow.add(vIri);

			//find the neighbors
			PreparedStatement ps = connection.prepareStatement(SQL_GET_NEIGHBOURS);
			ps.setString(1, vIri);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				String subj = rs.getString("subject_");
				String pred = rs.getString("predicate_");
				String obj = rs.getString("object_");

				Statement triple = BlazegraphUsefulMethods.createAStatement(subj, pred, obj);

				//add the triple to the subgraph
				subgraph.add(triple);

				//if we reached the limit of the radius, stop
				if(vRadius + 1 > this.tau)
					continue;

				//check the object
				String object = triple.getObject().stringValue();
				//add it to the queue if it is a IRI
				if(UrlUtilities.checkIfValidURL(object)) {
					VNode u = new VNode();
					u.setIri(object);
					u.setRadius(vRadius + 1);
					iris.add(u);
				}
			}
		}//end while

		//try to see if this graphs contains all the keywords
		//extrapolate the text from the graph
		String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(subgraph);
		//index this graph
		try {
			System.setProperty("tokeniser", "EnglishTokeniser");
			MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
			//check if all the query words are present in the document

			//get the lexicon
			Lexicon<String> lex = memIndex.getLexicon();
			//boolean to help us understand if this graph is fit to be returned
			boolean returning = true;

			for(String queryWord : queryWords) {
				LexiconEntry le = lex.getLexiconEntry(queryWord);
				if(le == null) {
					returning = false;
					break;
				}
			}

			if(returning) {
				//this is an answer graph (contains all the query words), and we can print it
				this.answerGraphCounter++;
				//remove from the query graph the nodes visited in this iteration
				exploredSubjects.removeAll(nodesVisitedNow);
				memIndex.close();
				return subgraph;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;


	}




	/** One of the two main methods of this class (method B)
	 * 
	 * Not exactly deprecated, but this implements TSA+BACK instead of TSA+VDP+BACK
	 * it is a simpler, probably more complex version.*/
	public void tsa_backtrack() {
		File f = new File(this.answerGraphOutputDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}
		try {
			FileUtils.cleanDirectory(f);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		//prepare the list of keywords from the query to use in this run
		System.setProperty("tokeniser", "EnglishTokeniser");
		this.queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);

		//path with the run file produced by the previous BM25 method
		Path path = Paths.get(this.runFile);
		try(BufferedReader reader = Files.newBufferedReader(path)) {
			String line = "";

			//counter to check that we do not go beyond the limit of graphs we read
			int counter = 0;
			while((line = reader.readLine()) != null && counter < limit) {

				//if time is due, need to wrap things up
				if(Thread.interrupted() || !ThreadState.isOnLine()) {
					ThreadState.setOnLine(false);
					continue;
				}

				counter++;
				String[] parts = line.split(" ");
				//take the id of the graph, we will use it to build the path 
				String id = parts[2];
				String graphPath = this.readingGraphsDirectory + "/" + id + ".ttl";
				//read the graph from disk
				Model graph = new TreeModel(BlazegraphUsefulMethods.readOneGraph(graphPath));

				//check this graph
				this.runOperationsOverOneBM25Graph(graph);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RDFParseException e) {
			e.printStackTrace();
		} catch (RDFHandlerException e) {
			e.printStackTrace();
		}

		//if we are here it means that the algorithm finished or the time went out
		//in any case, we write our answers
		Collections.sort(this.answerList, new SummDocumentComparator());
		this.printResults();
	}

	private void printResults() {
		File f = new File(this.resultOutputFile);
		f = new File(f.getParent());
		f.mkdir();
		
		Path p = Paths.get(this.resultOutputFile);
		try(BufferedWriter writer = Files.newBufferedWriter(p, UsefulConstants.CHARSET_ENCODING)) {
			for(int i = 0; i < this.answerList.size(); ++i) {
				SummDocument doc = this.answerList.get(i);
				writer.write("query_no " + this.queryId + 
						" " + doc.getDocno() + " " + i + 
						" " + doc.getScore() + " " + this.method);
				writer.newLine();
			}
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Given a graph as input, this method 
	 * executes the backtrack operation.
	 * 
	 * */
	private void runOperationsOverOneBM25Graph(Model graph) {

		//convert the graph in a document and check for the keywords
		boolean check = checkIfGraphContainsAllKeywords(graph);

		if(check) {
			//the graph contains all the keyword. We can operate
			this.backtrackInOne(graph);
		} else 
			return;

	}


	/** Executes the backtrack algorithm over one BM25+merge graph.
	 * <p>
	 * It is the implementation of the Backward algorithm presented in the
	 * SUMM paper
	 * 
	 * 
	 * */
	private void backtrackInOne(Model graph) {
		//initialization 

		//these are the a_i, the priority queues for the paths
		BackwardQueues queues = new BackwardQueues(this.queryWords.size());

		//these are the W_i, the sets of nodes containing the keywords
		Map<Integer, List<KeywordNode>> W = this.findKeywordNodesIn(graph);

		//initialization phase of the algorithm where we prepare the W_i and a_i
		this.initializationPhase(W, queues, graph);

		//completed the initialization phase, we can iterate
		this.iterativePhase(W, queues, graph);


	}

	private boolean checkIfGraphContainsAllKeywords(Model graph) {
		//transform the graph in document
		String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(graph);

		System.setProperty("tokeniser", "EnglishTokeniser");
		//get query words operated
		List<String> queryWords = TerrierUsefulMethods.getDocumentWordsWithTerrierAsList(this.query);
		try {
			//index the graph virtual document
			MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
			Lexicon<String> lex = memIndex.getLexicon();

			//for every keyword, check if it is contained
			for(String queryWord : queryWords) {
				//look into the lexicon if the 
				LexiconEntry le = lex.getLexiconEntry(queryWord);
				if(le == null) {
					return false; //keyword non present, we can return false since the graph does not contain one of them
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		//if we are here, we checked every keyword. We can return true
		return true;
	}

	/** Performs the iterative phase of the Backward
	 * algorithm. 
	 * <p>
	 * since we are in a simpler version of the algorithm in a subgraph,
	it makes sense to continue while you have at least one
	entry in all of the queues a_i, it means that you 
	have at least one leaf somewhere which still has to find a root
	and you can still create a tree with all the leaves. f at least one of the queues a_i 
	is empty, you can stop because you can not complete.

	@param W the set of sets W_i, one for each keyword, containing the keyword nodes
	@param queues the heaps a_i
	@param subgraph the graph where we are working

	 */ 
	private void iterativePhase(Map<Integer, List<KeywordNode>> W, BackwardQueues queues, Model subgraph) {

		while(! this.stoppingConditionBackward(queues)) {

			//line 9 of pseudocode
			BackwardT t = queues.retrieveSmallestT();
			//take the last node visited in this backtracking
			SummNode v = t.getV();
			//find all the neighbors of v that are meta-nodes in reverse
			//line 10 of pseudocode
			List<Statement> subjList = this.getRootNeighbors(v.getUrl_(), subgraph);
			for(Statement triple : subjList) {
				//for every triple, we make a backtrack step
				this.backwardStep(triple, t, subgraph, queues);
			}

			//now we check if some M[v] is completed and, in case, we recreate the graph and print it
			this.checkForCompletedTrees();
		}
	}

	/** Checks in all the M[v] if they are complete (i.e. they have a tree).
	 * In that case, it builds a graph, its score, prints it and returns 
	 * it.
	 * */
	private void checkForCompletedTrees() {
		//check every M[v] in M
		for(Entry<String, ExtendedBackwardTList> entry : M.entrySet()) {

			if(this.answerList.size() >= this.tooManyAnswersForThisAlgorithm) {
				return;
			}

			ExtendedBackwardTList M_v_ = entry.getValue();
			if(M_v_.checkIfComplete() && !M_v_.isHasBeenExplored()) {
				//if we completed the tree and we did not print it already
				this.dealWithOneMv(M_v_);
			}
		}
	}

	/** We found a complete and yet to be explored M[v]. We look 
	 * at all the possible combinations.
	 * */
	private void dealWithOneMv(ExtendedBackwardTList M_v_) {
		this.answersFromOneGraphCounter++;
		//we need to deal with all the possible combinations of branches
		int n = this.queryWords.size();
		int[] indexes = new int[n];
		for(int j = 0; j < n; ++j) {
			indexes[j] = 0;
		}

		int totalCombinations = 1;
		for(int i = 0; i < n; ++i) {
			totalCombinations *= M_v_.gettElements().get(i).size();
		}

		//		System.out.println("DEBUG: Backward found a complete M[v], possible combinations: " 
		//		+ totalCombinations);

		int answerCounter_ = 0;

		//initialize the combination of M[v] we are going to use
		while(true) {
			if(answerCounter_ >= this.tooManyAnswersForOneRoot) {
				M_v_.setHasBeenExplored(true);
				return;
			}

			BackwardTList M_v__ = new BackwardTList(n);
			M_v__.setV(M_v_.getV());
			for(int i = 0; i <n; ++i) {
				//get the element and insert it in order to build the combination
				BackwardT t = M_v_.gettElements().get(i).get(indexes[i]);
				M_v__.setTElement( t, i);
			}

			int next = n - 1;
			while(next >= 0 && indexes[next] + 1 >= M_v_.gettElements().get(next).size() ) {
				next--;
			}
			//when here, next points to the element in indices that needs to be incremented 
			//in order to get to the next value

			if(next < 0)//all the combinations have been explored
				break;

			//prepare the index for the next execution
			indexes[next]++;

			//set to 0 all the other indexes
			for(int i = next + 1; i < n; ++i) {
				indexes[i] = 0;
			}

			//now we deal with this combination
			this.dealWithACombination(M_v__);
			answerCounter_++;
		}
		M_v_.setHasBeenExplored(true);
	}


	private void dealWithACombination(BackwardTList M_v_) {
		//get the whole subgraph
		Model answerGraph = M_v_.buildTheTree();
		//get its score
		int answerScore = M_v_.getTheScore();
		M_v_.setHasBeenPrinted(true);
		//get the id of this document, i.e. the root
		String vUrl = M_v_.gettElements().get(0).getV().getUrl_();

		//now we print the graph and add it to the answer list
		this.dealWithTheAnswer(answerGraph, answerScore);
	}

	/** Prints the answer graph.
	 * 
	 * 
	 * */
	private void dealWithTheAnswer(Model answerGraph, int answerScore) {
		//get the offset for the directory where we are going to print
		int offset = (int) Math.ceil((double) this.answerCounter / 2048);
		this.answerCounter++;

		String directoryPath =  this.answerGraphOutputDirectory;
		File f = new File(directoryPath);
		f.mkdirs();

		String graphPath = directoryPath + "/" + this.answerCounter + ".ttl";
		//print the answer
		BlazegraphUsefulMethods.printTheDamnGraph(answerGraph, graphPath);

		//now, insert the id of this answer 
		SummDocument doc = new SummDocument();
		doc.setDocno(this.answerCounter+"");
		doc.setScore(answerScore);

		//add it to the list in order to order it later
		this.answerList.add(doc);
	}



	/** Given an object node, retrieves all the subjects of that node that are 
	 * roots. In a sense, this is the backtrack step.
	 * <p>
	 * */
	private List<Statement> getRootNeighbors(String object, Model subgraph) {
		URIImpl obj_ = new URIImpl(object);
		//get all the subjects
		Model m = subgraph.filter(null, null, obj_);
		Iterator<Statement> it = m.iterator();
		List<Statement> rList = new ArrayList<Statement>();
		while(it.hasNext()) {
			rList.add(it.next());
		}
		return rList;
	}

	/** performs one step of backward.
	 * 
	 * @param triple the triple whose subject is used to backtrack
	 * */
	private void backwardStep(Statement triple, BackwardT oldT, Model subgraph, BackwardQueues queues) {
		//we need to create a new t
		BackwardT t = new BackwardT();

		//set the first node (leaf)
		t.setU(oldT.getU());

		//set the path
		List<Statement> path = new ArrayList<Statement>(oldT.getPath());
		path.add(triple);
		t.setPath(path);

		//set the distance of this t
		t.setDistance(oldT.getDistance() + 1);

		//set the last node (root)
		SummNode v = new SummNode();
		v.setUrl_(triple.getSubject().stringValue());
		t.setV(v);

		//we update the branch - i.e. take the old one and add the new triples
		Model branch = new LinkedHashModel(oldT.getBranch());
		branch.add(triple);
		branch.addAll(this.getNonRootNeighbors(v.getUrl_(), subgraph));
		t.setBranch(branch);

		//now we update M and a_i
		this.updateM4IterativePhase(t, queues);
	}


	/** Updates M[v][i] and also a_i.
	 * 
	 * */
	private void updateM4IterativePhase(BackwardT t, BackwardQueues queues) {
		//get the value v of M[v]
		String v = t.getV().getUrl_();
		ExtendedBackwardTList M_v_ = M.get(v);

		if(M_v_ == null) {
			//root found for the first time,
			//create the row M[v]
			M_v_ = new ExtendedBackwardTList(this.queryWords.size());
		}

		//now find the i for M[v][i]
		int i = t.getU().getKeywordId();
		//do the update if necessary
		boolean updated = M_v_.updateTElement(t, i);

		if(updated)
			queues.addToQueue(t, i);
	}


	/** function that computes if we have to stop the execution in the 
	 * backward subgraph. This function returns true if we have to stop,
	 * false if we have to continue.
	 * <p>
	 * Also, it stops if too many subgraphs were generated in general (value k)
	 * <p>
	 * Also, it stops if too many graphs were generated from the 
	 * graph from the BM25+merge algorithm, due to the fact that too many quite
	 * similar alternatives may be generated
	 * */
	private boolean stoppingConditionBackward(BackwardQueues queues) {
		//		return this.queues.checkForEmptyQueue();

		//we produced too many answers for this subgraph
		if(this.answerList.size() >= this.tooManyAnswersForThisAlgorithm) {
			return true;
		}

		if(this.answersFromOneGraphCounter >= this.tooManyAnswersForThisSubgraph) {
			this.answersFromOneGraphCounter = 0;
			return true;
		}

		return queues.checkForAllEmptyQueues();
	}

	/** Initialization phase of the algorithm, that is, the starting
	 * phase. Corresponds to lines 3 to 7 of the Backward 
	 * pseudocode.
	 * 
	 * @param W the sets W_i of the starting nodes containing the keywords
	 * @param queues the priority queues a_i, one for each keyword
	 * @param subgraph the graph where we are working right now 
	 * 
	 * */
	private void initializationPhase(Map<Integer, List<KeywordNode>> W, BackwardQueues queues, Model subgraph) {
		//for every W_i
		for(Entry<Integer, List<KeywordNode>> entry : W.entrySet()) {
			//check if we can continue
			if(Thread.interrupted() || !ThreadState.isOnLine()) {
				ThreadState.setOffLine(false);
				return;
			}

			//get the list of keyword nodes
			List<KeywordNode> keynodeList = entry.getValue();
			//initialize over this set of nodes
			this.initializationPhaseOnOneSetOfKeynodes(keynodeList, subgraph, queues);
		}
	}


	/** Performs the execution of the initialization phase over one W_i,
	 * one set of keyword nodes. It represent one iteration of the lines 
	 * 4-7 of the Backward pseudocode in the SUMM paper.
	 * 
	 * @param keynodeList list of nodes in W_i
	 * @param subgraph the graph where we are working
	 * @param queues the a_i of the algorithm. These are priority queues representing the nodes
	 * that are roots of paths leading to a keynode containing the keyword w_i
	 * */
	private void initializationPhaseOnOneSetOfKeynodes(List<KeywordNode> keynodeList, Model subgraph, BackwardQueues queues) {
		//for every node v in this W_i
		for(KeywordNode n : keynodeList) {
			this.initializationPhaseOnOneLeafNode(n, subgraph, queues);
		}
	}


	/** Start the backward from one leaf node
	 * 
	 * @param n the keyword node leaf.
	 * @param subgraph the graph where we are working
	 * @param queues the priority queues a_i
	 * */
	private void initializationPhaseOnOneLeafNode(KeywordNode n, Model subgraph, BackwardQueues queues) {
		/*Since we know that
		 * this node correspond to a path of length 0
		for the keyword node n, we can immediately update M[n]*/
		this.insertSourceNodeInMv(n);

		//now we work on the incoming star of n
		//we set n as object, and we look for its incoming star
		URIImpl obj_ = new URIImpl(n.getNodeUrl());
		Model neighbors = subgraph.filter(null, null, obj_);
		//now, for each neighbor (line 4 of the pseudocode)
		Iterator<Statement> iterator = neighbors.iterator();
		while(iterator.hasNext()) {
			Statement triple = iterator.next();
			//create the new  element t
			BackwardT t = new BackwardT();
			t.setU(n);
			t.addTripleToPath(triple);
			t.setDistance(1);//the new node v is at distance 1 from leaf n

			String subject = triple.getSubject().stringValue();
			SummNode v = new SummNode();
			v.setUrl_(subject);
			t.setV(v);

			//now we add this t in the corresponding queue a_i
			queues.addToQueue(t, n.getKeywordId());
			//also, update this t with the information about the nodes around subject and object
			this.updateTWithBothObjectAndSubject(t, triple, subgraph);
			//also, update the corresponding M[v]
			this.updateM4InitializationPhase(t);
		}
	}

	/** Given a t, inserts in it the statement triple and its neighbors
	 * that are root nodes
	 * */
	private void updateTWithBothObjectAndSubject(BackwardT t, Statement triple, Model subgraph) {
		Model branch = t.getBranch();
		//first of all, add the triple
		branch.add(triple);

		//then add the neighbour of the subject
		String subject = triple.getSubject().stringValue();
		branch.addAll(this.getNonRootNeighbors(subject, subgraph));

		//do the same with the neighbor of the object
		String object = triple.getObject().stringValue();
		branch.addAll(this.getNonRootNeighbors(object, subgraph));

		t.setBranch(branch);
	}

	private void insertSourceNodeInMv(KeywordNode n) {
		//we prepare a branch object (t corresponds to the
		//quadruple of the pseudocode) to insert in M[n]

		//create the path object
		BackwardT t = new BackwardT();

		//set the leaf of this path to n
		t.setU(n);

		SummNode v = new SummNode();
		v.setUrl_(n.getNodeUrl());
		v.setId_(n.getNodeId());

		t.setV(v);
		t.setDistance(0);

		//this is the subgraph representing the branch/path with n as leaf
		Model branch = new LinkedHashModel();
		branch.addAll(n.getNeighbour());
		t.setBranch(branch);

		this.updateM4InitializationPhase(t);
	}


	/** This method is used to populate the structure M during the initialization phase
	 * (the process is a little different w.r.t when at regime).
	 * The object BackwardT represents a path from a leaf to a root.
	 * M[u], where u is the root, is initialized in this way with a path of length 0
	 * de facto, since root and leaf are the same in the initialization.
	 * */
	private void updateM4InitializationPhase(BackwardT t) {
		//we need to update M[v]
		String v = t.getV().getUrl_();
		ExtendedBackwardTList M_v_ =  M.get(v);
		if(M_v_ == null) {
			//root found for the first time,
			//create the row M[v]
			M_v_ = new ExtendedBackwardTList(this.queryWords.size());
		}

		//insert this t in M[v][i]
		boolean updated = M_v_.updateTElement(t, t.getU().getKeywordId());
		if(updated)
			M.put(v, M_v_);
	}

	/** Given a string which is the IRI of a node, returns a
	 * list of triples that are the non root neighbors
	 * (literal nodes or type nodes)*/
	private List<Statement> getNonRootNeighbors(String subject, Model subgraph) {
		URIImpl s = new URIImpl(subject);
		Model m = subgraph.filter(s, null, null);
		Iterator<Statement> it = m.iterator();
		List<Statement> returnList = new ArrayList<Statement>();

		while(it.hasNext()) {
			Statement t = it.next();
			String object = t.getObject().stringValue();
			if(!UrlUtilities.checkIfValidURL(object)) {
				//if is a literal, so add
				returnList.add(t);
			} else {
				//else, I need to check if it is a type
				String type = UrlUtilities.takeWordsFromIri(t.getPredicate().stringValue());
				if(type.equals("type")) {
					//type node, we add
					returnList.add(t);
				}
			}
		}//end while
		return returnList;
	}



	/** Given a graph, finds all the nodes containing at least one keyword
	 * <p>
	 * These are meta-nodes, that is, nodes with their outgoing star,  
	 * composed by literals and type
	 * <p>
	 * The query words are a field of the object
	 * <p>
	 * 
	 * */
	private Map<Integer, List<KeywordNode>> findKeywordNodesIn(Model subgraph) {
		//a map representing the sets W. Every set is a list of nodes,
		//every node is associated to a keyword 
		//through the index of that keyword
		Map<Integer, List<KeywordNode>> W = new HashMap<Integer, List<KeywordNode>>();

		//a list to keep track of all the subjects we already checked.
		List<String> checkedSubjects = new ArrayList<String>();

		//for every subject of the graph
		for(Statement t : subgraph) {
			String subject = t.getSubject().stringValue();
			if(checkedSubjects.contains(subject))
				continue;//already checked subject, go on

			//add to the list of checked subject or we are in deep troubles
			checkedSubjects.add(subject);

			//check the surrounding of this node
			Resource sbj = t.getSubject();
			Model neighbour = BlazegraphUsefulMethods.returnMetaNodeWithCenterThis(sbj, subgraph);
			//now check if this neighbour contains at least one keyword
			String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(neighbour);

			try {
				MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
				Lexicon<String> lex = memIndex.getLexicon();

				for(int i = 0; i < queryWords.size(); ++i) {
					LexiconEntry le = lex.getLexiconEntry(queryWords.get(i));
					if(le != null) {
						//found one
						//create the new node
						KeywordNode node = new KeywordNode();
						node.setNodeUrl(sbj.stringValue());
						node.setKeyword(queryWords.get(i));
						node.setKeywordId(i);
						node.setNeighbour(neighbour);

						//get the list of nodes already found for the keyword w_i
						List<KeywordNode> list = W.get(i);
						if(list==null) {
							//first time we meet this keyword
							list = new ArrayList<KeywordNode>();
							list.add(node);
						}
						else {
							list.add(node);
						}

						W.put(i, list);

					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}//end for
		return W;

	}


	/** A slight modification over the method of the superclass since here we are
	 * not printing the graph on disk but we will keep it in memory, issuing a maximum number
	 * of considered graphs of 1000. Why 1000 you may ask. 
	 * This is due to the fact that usually no one goes beyond 1000. And every graph
	 * found here will get us at least one answer. Since we will always limit ourselves
	 * with the top x answers, likely the top 5 or 10, there is no need to go very 
	 * long in the run. (the truth is I did not wanted to put a property defined threshold,
	 * but do not tell anyone please).
	 * 
	 * <p>
	 * 
	 * 
	 * */
	protected Model dealWithOneRootAndReturnIt(String subject, Model graph, List<String> queryWords) throws SQLException {
		//graph that we are trying to build here. If it will contain all the keywords, it will be kept
		Model subgraph = new TreeModel();

		//perform a BFS algorithm
		VNode source = new VNode();
		source.setIri(subject);
		source.setRadius(0);

		//a queue that contains IRIs to visit
		Queue<VNode> iris = new LinkedList<VNode>();
		iris.add(source);

		while(!iris.isEmpty()) {
			VNode v = iris.remove();
			String vIri = v.getIri();
			int vRadius = v.getRadius();

			//find the neighbors
			PreparedStatement ps = connection.prepareStatement(SQL_GET_NEIGHBOURS);
			ps.setString(1, vIri);
			ResultSet rs = ps.executeQuery();

			while(rs.next()) {
				String subj = rs.getString("subject_");
				String pred = rs.getString("predicate_");
				String obj = rs.getString("object_");

				Statement triple = BlazegraphUsefulMethods.createAStatement(subj, pred, obj);

				//add the triple to the subgraph
				subgraph.add(triple);

				//if we reached the limit of the radius, stop
				if(vRadius + 1 > this.tau)
					continue;

				//check the object
				String object = triple.getObject().stringValue();
				//add it to the queue if it is a IRI
				if(UrlUtilities.checkIfValidURL(object)) {
					VNode u = new VNode();
					u.setIri(object);
					u.setRadius(vRadius + 1);
					iris.add(u);
				}
			}
		}//end while

		//now we have the whole graph

		//extrapolate the text from the graph
		String graphDocument = BlazegraphUsefulMethods.fromGraphToDocument(subgraph);

		System.setProperty("tokeniser", "EnglishTokeniser");
		try {
			MemoryIndex memIndex = TerrierUsefulMethods.getMemoryIndexFromDocument(graphDocument);
			//check if all the query words are present in the document

			//get the lexicon
			Lexicon<String> lex = memIndex.getLexicon();
			//boolean to help us understand if this graph is fit to be returned
			boolean returning = true;

			for(String queryWord : queryWords) {
				LexiconEntry le = lex.getLexiconEntry(queryWord);
				if(le == null) {
					returning = false;
					break;
				}
			}

			//if we are here, it means that the graph has all the query words
			if(returning) {
				return subgraph;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}










	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public int getTooManyAnswersForThisSubgraph() {
		return tooManyAnswersForThisSubgraph;
	}

	public void setTooManyAnswersForThisSubgraph(int tooManyAnswersForThisSubgraph) {
		this.tooManyAnswersForThisSubgraph = tooManyAnswersForThisSubgraph;
	}

	public int getTooManyAnswersForOneRoot() {
		return tooManyAnswersForOneRoot;
	}

	public void setTooManyAnswersForOneRoot(int tooManyAnswersForOneRoot) {
		this.tooManyAnswersForOneRoot = tooManyAnswersForOneRoot;
	}



}
