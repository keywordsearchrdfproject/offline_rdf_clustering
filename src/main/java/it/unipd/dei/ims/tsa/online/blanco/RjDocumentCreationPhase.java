package it.unipd.dei.ims.tsa.online.blanco;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Queue;

import org.apache.commons.io.FileUtils;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.Rio;
import org.openrdf.rio.helpers.StatementCollector;
import org.terrier.indexing.TRECCollection;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.rum.utilities.PropertiesUsefulMethods;
import it.unipd.dei.ims.rum.utilities.UrlUtilities;
import it.unipd.dei.ims.rum.utilities.UsefulConstants;
import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.TerrierUsefulMethods;
import it.unipd.dei.ims.tsa.offline.IndexerDirectoryOfTRECFiles;

/** TSA Algorithm: phase 8 (+Blanco). 
 * <p>
 * Here we create the R_j files and the different indexes that we need.
 * We also create a list of found predicates that are useful for the ranking phase.
 * */
public class RjDocumentCreationPhase {
	
	private String terrierHome, terrierEtc;
	
	/** Directory where the answer graphs are stored
	 * <p>
	 * property: blanco.subgraphs.directory*/
	private String answerSubgraphsDirectory;
	
	/** Directory where to save the R_j files
	 * <p>
	 * property: blanco.r.j.output.directory 
	 * */
	private String rjOutputDirectory;
	
	
//	private String rjFilesDirectory;
	
	/** Lenght of a key for Terrier. */
	private String keylens;
	
	
	
	public RjDocumentCreationPhase () {
		
		try {
			this.setup();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setup() throws IOException {
		Map<String, String> map = 
				PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		
		this.terrierHome = map.get("terrier.home");
		this.terrierEtc = map.get("terrier.etc");
		//set the needed properties
		System.setProperty("terrier.home", terrierHome);
		System.setProperty("terrier.etc", terrierEtc);
		
		this.keylens = map.get("keylens");
		
		this.answerSubgraphsDirectory= map.get("blanco.subgraphs.directory");
		this.rjOutputDirectory = map.get("blanco.r.j.output.directory");
		
	}
	
	/** Creates the R_j files provided the collection of answers documents
	 * and builds the necessary indexes.
	 * */
	public void createTheRjFiles() {
		try {
			this.buildTheRjFiles();
			this.createTheIndexes();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void buildTheRjFiles () throws IOException {
		//make sure the directory exists
		File f = new File(rjOutputDirectory);
		if(!f.exists()) {
			f.mkdirs();
		}
		
		//clean the directory
		FileUtils.cleanDirectory(new File(rjOutputDirectory));
		
		int counter = 0;

		//directory where the collection is stored as RDF files (necessary to have the structure to build the Rj)
		File subgraphDirectoryFile = new File(this.answerSubgraphsDirectory);

		Queue<File> subgraphDirQueue = new LinkedList<File>();
		Map<String, PrintWriter> rjMap = new HashMap<String, PrintWriter>();

		//traverse the tree of files in pre-order
		subgraphDirQueue.add(subgraphDirectoryFile);
		while(!subgraphDirQueue.isEmpty()) {
			f = subgraphDirQueue.remove();
			File[] files = f.listFiles();
			for(File file : files) {
				if(file.isDirectory()) {
					subgraphDirQueue.add(file);
				} else {
					//it is a graph file
					String name = file.getName();
					if(name.equals(".DS_Store")) {
						continue;
					}
					addOneFileToRj(file, rjMap, rjOutputDirectory);
					counter++;
					if(counter%2048 == 0)
						System.out.println("examined " + counter + " graphs");
				}
			}
		}
		
		//print a list with all the predicates we have seen
		File d = new File(rjOutputDirectory).getParentFile();
		Path path = Paths.get(d.getAbsolutePath() + "/predicate_list.txt");
		BufferedWriter writer = Files.newBufferedWriter(path, UsefulConstants.CHARSET_ENCODING);

		//clean everything
		for(Entry<String, PrintWriter> entry : rjMap.entrySet()) {
			//print a line of the list
			String predicate = entry.getKey();
			writer.write(predicate);
			writer.newLine();
			
			//close one of the PrintWriter
			PrintWriter pw = entry.getValue();
			pw.println("");
			pw.print("</DOC>");
			pw.close();
		}
		
		writer.close();
	}
	
	/** Given the path of a RDF file, use it to increment the R_j documents.
	 * 
	 * @param rjMap a map containing the path for each file rj
	 * @param rjDirectory path to the directory where to save the files Rj
	 * */
	private static void addOneFileToRj(File graphFile, Map<String, PrintWriter> rjMap, String outputRjDirectory) {

		//read the file and incorporate in a model
		InputStream inputStream;
		try {
			File o1 = new File(outputRjDirectory);
			if(!o1.exists())
				o1.mkdirs();
			
			
			inputStream = new FileInputStream(graphFile);
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();
			Iterator<org.openrdf.model.Statement> iter = statements.iterator();
			
			inputStream.close();

			while(iter.hasNext()) {
				//get the predicate r of the triple
				Statement t = iter.next();
				URI predicate = t.getPredicate();
				if(! rjMap.containsKey(predicate.toString()) ) {//first time we see this r
					//obtain the document connected to the statement
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read through Terrier
					String title = UrlUtilities.takeWordsFromIri(predicate.toString());

					//create a new file for this Rj document 
					String output = outputRjDirectory + "/" + title + ".trec";
					File o = new File(output);
					if(!o.exists())//create a new R_j file named with the predicate
						o.createNewFile();

					FileWriter fw = new FileWriter(output, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw);
					
					if(title.length() > 20) {
						title = title.substring(0, 19);
					}
					//open the document and write the first words
					out.println("<DOC>");
					out.println("<DOCNO>" + /*predicate.toString()*/ title + "</DOCNO>");
					out.print(tripleDoc + " ");

					//add the printer to the map
					rjMap.put(predicate.toString(), out);
					out.flush();
				} else {
//					predicate already present
					PrintWriter out = rjMap.get(predicate.toString()); 

					//get document of this triple
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read throug Terrier
					String tDocTer = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDoc);
					//add the line to the file
					out.print(" " + tDocTer);
					out.flush();
				}

			}
			
		} catch ( RDFParseException | RDFHandlerException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/** Given the path of a RDF file, use it to increment the R_j documents.
	 * 
	 * @param rjMap a map containing the path for each file rj
	 * @param rjDirectory path to the directory where to save the files Rj
	 * */
	private static void addOneFileToRjRedacted(File graphFile, Map<String, PrintWriter> rjMap, String outputRjDirectory) {

		//read the file and incorporate in a model
		InputStream inputStream;
		try {
			File o1 = new File(outputRjDirectory);
			if(!o1.exists())
				o1.mkdirs();
			
			
			inputStream = new FileInputStream(graphFile);
			//prepare a collector to contain the triples
			StatementCollector collector = new StatementCollector();
			//read the file
			RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
			//link the collector to the parser
			rdfParser.setRDFHandler(collector);
			//parse the file
			rdfParser.parse(inputStream, "");
			//now get the triples/statements composing the graph
			Collection<org.openrdf.model.Statement> statements = collector.getStatements();
			Iterator<org.openrdf.model.Statement> iter = statements.iterator();
			
			inputStream.close();

			while(iter.hasNext()) {
				//get the predicate r of the triple
				Statement t = iter.next();
				URI predicate = t.getPredicate();
				if(! rjMap.containsKey(predicate.toString()) ) {//first time we see this r
					//obtain the document connected to the statement
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read through Terrier
					String title = UrlUtilities.takeWordsFromIri(predicate.toString());

					//create a new file for this Rj document 
					String output = outputRjDirectory + "/" + title + ".trec";
					File o = new File(output);
					if(!o.exists())//create a new R_j file named with the predicate
						o.createNewFile();

					FileWriter fw = new FileWriter(output, true);
					BufferedWriter bw = new BufferedWriter(fw);
					PrintWriter out = new PrintWriter(bw);
					
					if(title.length() > 20) {
						title = title.substring(0, 19);
					}
					//open the document and write the first words
					out.println("<DOC>");
					out.println("<DOCNO>" + /*predicate.toString()*/ title + "</DOCNO>");
					out.print(tripleDoc + " ");

					//add the printer to the map
					rjMap.put(predicate.toString(), out);
					out.flush();
				} else {
//					predicate already present
					PrintWriter out = rjMap.get(predicate.toString()); 

					//get document of this triple
					String tripleDoc = BlazegraphUsefulMethods.fromStatementToDocument(t);
					//index it and obtained the version read throug Terrier
					String tDocTer = TerrierUsefulMethods.getDocumentWordsWithTerrierAsString(tripleDoc);
					//add the line to the file
					out.print(" " + tDocTer);
					out.flush();
				}

			}
			
			for(Entry<String, PrintWriter> entry : rjMap.entrySet()) {
				if(entry.getValue()!=null) {
					entry.getValue().close();
				}
			}
		} catch ( RDFParseException | RDFHandlerException | IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/** Creates one index for each document R_j in a separated directory. 
	 * Moreover, creates one index of the whole collection.
	 * <p>
	 * This method utilizes the path of the directory where are stored 
	 * the R_j files in order to obtain the paths to the indexes it creates.
	 * 
	 * */
	private void createTheIndexes() throws IOException {
		System.out.println("creating the support indexes, please wait...");
		
		//need to put a little bit of more space for the keys
		//XXX you need to put these 2 lines AFTER the creation of the documents for technical reasons (it's a kind of magic)
//		System.setProperty("indexer.meta.forward.keylens", keylens);
//		System.setProperty("indexer.meta.forward.keys", "DOCNO");
		
		//now create 1 index for each file
		File rjFir = new File(this.rjOutputDirectory);

		String indexesDirectory = rjFir.getParent() + "/rj_indexes";
		String inedexDirectory = rjFir.getParent() + "/rj_index";
		
		File[] files = rjFir.listFiles();
		int indexCounter = 0;
		
		//index each and every R_j file
		for(File file : files) {
			//for each file, create the corresponding index
			if(file.getName().equals(".DS_Store"))
				continue;
			InputStream in = new FileInputStream(file);
			String outputDirIndex = indexesDirectory + "/" + file.getName().replaceAll("\\.trec", "");
			File dir = new File(outputDirIndex);
			if(!dir.exists())
				dir.mkdirs();
			
			//index the single R_j
			//need two object because one indexing set the object in a state where we cannot use it.
			org.terrier.indexing.Collection col = new TRECCollection(in);
			
			//XXX pay attention: these are indexes of the single documents. 
			//we use them later in the adapted Language Model.
			//you have to check that also the collective index is generated
			Indexer indexer = new BasicIndexer(outputDirIndex, "data");
			indexer.index(new org.terrier.indexing.Collection[]{ col });
//			colList.add(col);
			in.close();
			indexCounter++;
		}
		
		System.out.println("single indexes created. They are: " + indexCounter);
		
		
		//create the directory where to store the index if not present
		File indxDir = new File(inedexDirectory);
		if(!indxDir.exists())
			indxDir.mkdirs();
		
		System.out.println("creating the complete index of R_j...");

		//prepare the indexer
		IndexerDirectoryOfTRECFiles wholeIndexer = new IndexerDirectoryOfTRECFiles();
		wholeIndexer.setDirectoryToIndex(rjOutputDirectory);
		wholeIndexer.setIndexPath(inedexDirectory);
		wholeIndexer.index("unigram");
		
		
		//put things back together
//		System.setProperty("indexer.meta.forward.keys", "DOCNO");
		
	}
	
	/** Test main*/
	public static void main(String[] args) {
		RjDocumentCreationPhase phase = new RjDocumentCreationPhase();
		phase.createTheRjFiles();
	}

	public String getAnswerSubgraphsDirectory() {
		return answerSubgraphsDirectory;
	}

	public void setAnswerSubgraphsDirectory(String answerSubgraphsDirectory) {
		this.answerSubgraphsDirectory = answerSubgraphsDirectory;
	}

	public String getRjOutputDirectory() {
		return rjOutputDirectory;
	}

	public void setRjOutputDirectory(String rjOutputDirectory) {
		this.rjOutputDirectory = rjOutputDirectory;
	}
	
	
	
}
