package it.unipd.dei.clustering.disgenet;

import org.apache.log4j.Logger;

import it.unipd.dei.ims.clustering.MainClass;

/**This is a particular class created with the purpose to cluster the DisGeNET 
 * database. This is due to the fact that DisGeNET is divided in various 
 * graph files. The approach used in this class can be used
 * in other classes that need to read databases contained in more than 1 dataset.
 * */
public class DisGeNETClusterMain {

	static Logger log = Logger.getLogger(MainClass.class);
	
	public static void main(String[] args) {
		System.out.println("currently clustering DisGeNET");
		
	}
}
