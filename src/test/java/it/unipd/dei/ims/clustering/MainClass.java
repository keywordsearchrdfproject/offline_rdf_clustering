package it.unipd.dei.ims.clustering;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;
import org.apache.log4j.Logger;

import it.unipd.dei.ims.clustering.utilities.SimpleDocumentBuilder;
import it.unipd.dei.ims.clustering.utilities.UsefulMethods;
import it.unipd.dei.ims.statistics.Statistics;


/**This is the starting class of the offline_clustering project.
 * It reads and clusters a particular database following the information
 * in the properties
 * */
@Deprecated
public class MainClass {

	static Logger log = Logger.getLogger(MainClass.class);

	public static void main(String[] args) {
		System.out.println("Starting the offline clustering");
		Stopwatch timer = Stopwatch.createStarted();

		//map to contain the preferences
		Map<String, String> propertyMap = UsefulMethods.getProperties();

		String testFlag = propertyMap.get("are.we.testing.label");
		//read the graph/model
		String graphPath;
		if(testFlag.equals("true")) {
			graphPath = propertyMap.get("test.graph.path");
		}
		else
			graphPath = propertyMap.get("graph.path");

		InputStream in = FileManager.get().open(graphPath);
		if(in == null) {
			//in case the file is missing
			throw new IllegalArgumentException("File " + graphPath +" not found" );
		}
		//READ THE GRAPH 
		Model model = ModelFactory.createDefaultModel();
		
		String label = propertyMap.get("are.we.reading.the.graph.label");
		if(label.equals("true")) {
			System.out.println("reading the model ...");
			model.read(in, null, propertyMap.get("model.syntax"));//reads the model supposing it is in .nt syntax
			System.out.println("Model read in " + timer.stop());
			try {
				in.close();
			} catch (IOException e1) {
				System.err.println("Error closing the input stream");
				e1.printStackTrace();
			}
		}


		//COMPUTE STATISTICS ABOUT THE GRAPH
		System.out.println("Producing graph's statistics");
		timer.start();
		Statistics stat = new Statistics(model);
		stat.getModelStatistics();
		System.out.println("Statistics produced in " + timer.stop());

		timer.start();
		//find the labels that we will traverse
		List<String> connectivityList = stat.getKConnectivityList(Integer.parseInt(propertyMap.get("connectivity.list.k")),
																propertyMap.get("connectivity.list.measure"));

		//Get some parameters of the algorithm from the properties
		String lambda_in = propertyMap.get("lambda.in");
		String lambda_out = propertyMap.get("lambda.out");
		String t_au = propertyMap.get("tau");
		int lin = 0;
		int lou = 0;
		int tau = 0;
		try {
			lin = Integer.parseInt(lambda_in);
			lou = Integer.parseInt(lambda_out);
			tau = Integer.parseInt(t_au);
		}
		catch(NumberFormatException e) {
			System.err.println("lambda in or lambda out or tau erroneously written in the properties");
		}

		//prepare the graph to be divided in clusters
		label = propertyMap.get("are.we.clustering.label");
		if(label.equals("true")) {
			ClusterEngine engine = new ClusterEngine(model, lin, lou);
			engine.computeTerminalAndSourceNodesSets(stat);
			System.out.println("Cluster engine setup at " + timer.stop());
			
			//Create the clusters - here the magic happens
			timer.start();
			//TODO sistemare questa cosa della cartella di destinazione
			String destination = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/clusters/clusters";
			engine.clustering(tau, connectivityList, destination);
			System.out.println("clustering finished at " + timer.stop());
		}

		//now we transform the clusters in documents
		label = propertyMap.get("are.we.translating.from.clusters.to.documents.label");
		if(label.equals("true")) {
			System.out.println("Starting converting the clusters in documents in " + timer.start());
			SimpleDocumentBuilder documentBuilder = new SimpleDocumentBuilder();
			documentBuilder.translateGraphsToXmlDocuments();
			System.out.println("Documents written at " + timer.stop());
		}
		
		
//		System.out.println("done in " + timer.stop());

	}


}


