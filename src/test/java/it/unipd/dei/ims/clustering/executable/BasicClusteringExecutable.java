package it.unipd.dei.ims.clustering.executable;

import java.util.List;
import java.util.Map;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;

import it.unipd.dei.ims.clustering.ClusterEngine;
import it.unipd.dei.ims.statistics.Statistics;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** In this class we read one database and we create its clusters
 * reading from a TDB dataset.
 * 
 * We suppose that the default model contains all the triples.
 * 
 * Using Jena.
 * */
@Deprecated
public class BasicClusteringExecutable {

	public static void main(String[] args) {
		Map<String, String> propertyMap = PropertiesUsefulMethods.getProperties();
		
		//open the database
//		String databaseDirectory = propertyMap.get("tdb.database.directory");
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB";
		Dataset dataset = TDBFactory.createDataset(databaseDirectory);
		
		//open read transaction
		dataset.begin(ReadWrite.READ);
		try {
			//take the (default) model
			Model model = dataset.getDefaultModel();
			System.out.println("Default Model Opened");
			
			//compute the statistics
			Statistics stat = new Statistics(model);
			stat.getModelStatistics();
			System.out.println("Statistics done");
			
			//computer the connectivity list
			int k = Integer.parseInt(propertyMap.get("k"));
			String measure = propertyMap.get("measure");
			List<String> connectivityList = stat.getKConnectivityList(k, measure);
			System.out.println("connectivity list done");
			
			//create the cluster engine and the central nodes
			int lambda_in = Integer.parseInt(propertyMap.get("lambda.in"));
			int lambda_out = Integer.parseInt(propertyMap.get("lambda.out"));
			ClusterEngine engine = new ClusterEngine(model, stat, lambda_in, lambda_out);
			engine.computeTerminalAndSourceNodesSets(stat);
			System.out.println("Engine setup completed, now clustering...");
			
			//now clustering
			String destinationDirectory = propertyMap.get("clusters.directory");
			int tau = Integer.parseInt(propertyMap.get("tau"));
			engine.clustering(tau, connectivityList, destinationDirectory);
			System.out.print("clustering has been completed");
			
			
		} finally {
			dataset.end();
		}
	}
}
