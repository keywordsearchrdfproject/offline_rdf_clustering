package it.unipd.dei.ims.clustering.executable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.RepositoryException;

import it.unipd.dei.ims.clustering.BlazegraphClusterEngineWithDatabaseSupport;
import it.unipd.dei.ims.statistics.BlazegraphStatisticsWithDatabaseSupport;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

/** In this class we read a dataset with Blazegraph and we create its clusters.
 * 
 * NB: it is required to have previously computed 
 * the statistics of the graph via {@link StatisticsExecutable}
 * 
 * */
public class ClusteringExecutable {
	
	public static void main(String[] args) throws RepositoryException, QueryEvaluationException, IOException {
		
		Map<String, String> propertyMap = PropertiesUsefulMethods.getSinglePropertyFileMap("properties/main.properties");
		
		//get the database path
		String databaseFile = propertyMap.get("rdf.database.file");
		String jdbcString = propertyMap.get("jdbc.connectivity.string");
		String clusterDirectory = propertyMap.get("clusters.directory");
		boolean labelFlag = Boolean.parseBoolean(propertyMap.get("label.flag"));
		
		///XXX DEBUG
//		databaseFile = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/rdf_database_spaces/database.jnl";
//		jdbcString = "jdbc:postgresql://localhost:5432/DisGeNET_test?user=postgres&password=Ulisse92";
//		clusterDirectory = propertyMap.get("clusters.directory.test");
		
		try {
			//compute connectivity list
			int k = Integer.parseInt(propertyMap.get("k"));
			int threshold = Integer.parseInt(propertyMap.get("labels.threshold"));
			List<String> connectivityList = BlazegraphStatisticsWithDatabaseSupport.getKConnectivityList(jdbcString, threshold, k);
			System.out.println("connectivity list computed " + connectivityList);
			
			//create the cluster engine 
			int lambda_in = Integer.parseInt(propertyMap.get("lambda.in"));
			int lambda_out = Integer.parseInt(propertyMap.get("lambda.out"));
			int tau = Integer.parseInt(propertyMap.get("tau"));
			BlazegraphClusterEngineWithDatabaseSupport engine = new BlazegraphClusterEngineWithDatabaseSupport();
			//set the engine parameters
			engine.setRepository(databaseFile);//rdf database
			engine.setJdbcString(jdbcString);
			engine.setLambda_in(lambda_in);
			engine.setLambda_out(lambda_out);
			engine.setDestinationDirectory(clusterDirectory);
			
			//clustering
			System.out.println("now clustering...");
			engine.clusterAlgorithm(tau, connectivityList, labelFlag);
			System.out.print("clustering has been completed");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			
		}
		
	}
}
