package it.unipd.dei.ims.clustering.executable.test;

import java.util.Map;
import java.util.Properties;

import org.openrdf.query.QueryEvaluationException;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.statistics.BlazegraphStatistics;
import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

public class BlazegraphClusteringExecutableTest {

	public static void main(String[] args) throws RepositoryException, QueryEvaluationException {
		Map<String, String> propertyMap = PropertiesUsefulMethods.getProperties();
		
		//get the database path
//		String databaseDirectory = propertyMap.get("tdb.database.directory");
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame_test/test.jnl";
		
		try {
			Properties props = new Properties();
			props.put(Options.BUFFER_MODE, "DiskRW");
			props.put(Options.FILE, databaseDirectory);

			final BigdataSail sail = new BigdataSail(props); // instantiate a sail
			final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository
			
			//compute the statistics
			BlazegraphStatistics stat = new BlazegraphStatistics();
			stat.getGraphStatistics(databaseDirectory);
			System.out.println("Statistics done");
			
			return;
			
//			//compute connectivity list
//			int k = Integer.parseInt(propertyMap.get("k"));
//			String measure = propertyMap.get("measure");
//			List<String> connectivityList = stat.getKConnectivityList(k, measure);
//			System.out.println("connectivity list computed");
//			
//			//create the cluster engine and find the central nodes
//			int lambda_in = Integer.parseInt(propertyMap.get("lambda.in"));
//			int lambda_out = Integer.parseInt(propertyMap.get("lambda.out"));
//			BlazegraphClusterEngine engine = new BlazegraphClusterEngine(databaseDirectory, lambda_in, lambda_out);
//			engine.computeTerminalAndSourceNodesSets(stat);
//			System.out.println("engine setup completed, now clustering...");
//			
//			//now clustering
//			String destinationDirectory = propertyMap.get("clusters.directory.test");	
//			int tau = Integer.parseInt(propertyMap.get("tau"));
//			engine.clustering(tau, connectivityList, destinationDirectory);
//			System.out.print("clustering has been completed");
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			
		}
		
	}
}
