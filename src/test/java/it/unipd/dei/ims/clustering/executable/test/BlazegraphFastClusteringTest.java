package it.unipd.dei.ims.clustering.executable.test;

import java.util.Map;
import java.util.Properties;

import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.QueryLanguage;
import org.openrdf.query.TupleQuery;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.terrier.utilities.PropertiesUsefulMethods;

public class BlazegraphFastClusteringTest {

	public static void main(String[] args) throws RepositoryException {

		final Properties props = new Properties();
		props.put(Options.BUFFER_MODE, "DiskRW"); // persistent file system located journal
		props.put(Options.FILE, "/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/TDB_sesame/test.jnl"); // journal file location

		final BigdataSail sail = new BigdataSail(props); // instantiate a sail
		final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository

		repo.initialize();

		try {
			// open repository connection
			RepositoryConnection cxn;

			// open connection
			if (repo instanceof BigdataSailRepository) {
				cxn = ((BigdataSailRepository) repo).getReadOnlyConnection();
			} else {
				cxn = repo.getConnection();
			}

			// evaluate sparql query
			try {

				final TupleQuery tupleQuery = cxn
						.prepareTupleQuery(QueryLanguage.SPARQL,
//								"select ?s ?p ?o where { ?s ?p ?o . }");
								"SELECT (count(*) AS ?count) { ?s ?p ?o .}");
				
				TupleQueryResult result = tupleQuery.evaluate();
				int counter = 0;
				try {
					while (result.hasNext()) {
						BindingSet bindingSet = result.next();
						counter++;
						System.err.println(bindingSet);
					}
					System.out.println("triples: " + counter);
				} finally {
					result.close();
				}

			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				// close the repository connection
				cxn.close();
			}

		} finally {
			repo.shutDown();
		}
	}
}
