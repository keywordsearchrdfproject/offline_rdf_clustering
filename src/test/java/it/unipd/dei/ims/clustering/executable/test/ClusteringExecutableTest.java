package it.unipd.dei.ims.clustering.executable.test;

import java.util.List;

import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;

import it.unipd.dei.ims.clustering.ClusterEngine;
import it.unipd.dei.ims.statistics.Statistics;

/** In this class we read one database and we create its clusters.
 * */
public class ClusteringExecutableTest {

	public static void main(String[] args) {
		//open the database
		String databaseDirectory = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/TDB_test";
		Dataset dataset = TDBFactory.createDataset(databaseDirectory);
		
		//open transaction of read
		dataset.begin(ReadWrite.READ);
		try {
			//XXX here we are using the Default Model
			Model model = dataset.getDefaultModel();
			Statistics stat = new Statistics(model);
			stat.getModelStatistics();
			System.out.println("Statistics done");
			
			List<String> connectivityList = stat.getKConnectivityList(10, "f-measure");
			
			ClusterEngine engine = new ClusterEngine(model, 2, 2);
			engine.computeTerminalAndSourceNodesSets(stat);
			String destination = "/Users/dennisdosso/Documents/RDF_DATASETS/IMDB/clusters/clusters/";
			engine.clustering(2, connectivityList, destination);
			
			
			
		} finally {
			dataset.end();
		}
	}
}
