package it.unipd.dei.ims.clustering.indexing;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.terrier.indexing.Collection;
import org.terrier.indexing.SimpleXMLCollection;
import org.terrier.matching.ResultSet;
import org.terrier.querying.Manager;
import org.terrier.querying.SearchRequest;
import org.terrier.structures.Index;
import org.terrier.structures.IndexOnDisk;
import org.terrier.structures.indexing.Indexer;
import org.terrier.structures.indexing.classical.BasicIndexer;

import it.unipd.dei.ims.clustering.utilities.UsefulMethods;

/**In this class we index and rank the XML documents 
 * which describes the clusters.
 * */
public class IndexAndRanking {

	private final static Charset ENCODING = StandardCharsets.UTF_8;

	public static void indexAndRank (Map<String, String> propertyMap) {
		//set some property to help terrier working as I wish
		System.setProperty("terrier.home", propertyMap.get("terrier.home"));
		//in the /etc directory it is present the terrier.properties file. Very useful, indeed
		System.setProperty("terrier.etc", propertyMap.get("terrier.etc"));

		String label = propertyMap.get("are.we.indexing.label");
		if(label.equals("true")) {
			//we get all the files we want to index
			List<String> filesToIndex = getListOfFilesToIndex(propertyMap);

			//collection of the xml files
			Collection collection = new SimpleXMLCollection(filesToIndex);

			//index the collection
			Indexer indexer = new BasicIndexer(propertyMap.get("index.path"), propertyMap.get("index.name"));
			indexer.index(new Collection[]{ collection });
		}

		//open the new index
		Index index = IndexOnDisk.createIndex(propertyMap.get("index.path"), propertyMap.get("index.name"));
		System.out.println("We have indexed " + index.getCollectionStatistics().getNumberOfDocuments() + " documents");

		//prepare the query
		Manager queryingManager = new Manager(index);
		SearchRequest srq = queryingManager.newSearchRequestFromQuery(propertyMap.get("query"));

		//choose the ranking model
		srq.addMatchingModel("Matching", propertyMap.get("ranking.model"));

		queryingManager.runSearchRequest(srq);

		ResultSet results = srq.getResultSet();
		System.out.println(results.getExactResultSize()+" documents were scored");
		System.out.println("The top "+results.getResultSize()+" of those documents were returned");

		// Print the results
		Path outputPath = Paths.get(propertyMap.get("result.ranking.file.path"));

		try (BufferedWriter writer = Files.newBufferedWriter(outputPath, ENCODING)){
			System.out.println("Document Ranking");
			for (int i =0; i< results.getResultSize(); i++) {
				int docid = results.getDocids()[i];

				String docno = index.getMetaIndex().getItem("docno", docid);
				double score = results.getScores()[i];
				writer.write("query_no Q0 " + docno + " " + i + " " + score + " " + propertyMap.get("ranking.model"));
				writer.newLine();
				System.out.println("   Rank "+i+" docno: "+ docno + " "+" "+score);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("done");
	}

	/**Returns a list of Strings containing the absolute paths
	 * of all the xml files that we want to index in this project.
	 * 
	 * @param propertyMap contains the path where to find the files.
	 * */
	private static List<String> getListOfFilesToIndex(Map<String, String> propertyMap) {
		//get the path
		File mainDirectoryPath = new File(propertyMap.get("clusters.turned.to.documents.directory"));
		//list all the files in this directory
		File[] files = mainDirectoryPath.listFiles();

		List<String> fileList = new ArrayList<String>();
		for(File file : files) {
			fileList.add(file.getPath());
		}
		//order the list (because I like it better ordered) and return it
		Collections.sort(fileList);
		return fileList;
	}


	public static void main(String[] args) {

		Map<String, String> propertyMap = UsefulMethods.getProperties();
		indexAndRank(propertyMap);
	}

}
