package it.unipd.dei.ims.reading.answer;

import java.io.InputStream;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;

/** Just a little class to make sure that the answers can be read
 * by Jena
 * */
public class AnswerReader {

	public static void main(String[] args) {

		InputStream in = FileManager.get().open("/Users/dennisdosso/Documents/RDF_DATASETS/DisGeNET/ground_truth/groundtruth1-1.ttl");
		//READ THE GRAPH 
		Model model = ModelFactory.createDefaultModel();
		model.read(in, null, "TURTLE");
		
		System.out.println("done");
		//t'appoooooooo
	}

}
