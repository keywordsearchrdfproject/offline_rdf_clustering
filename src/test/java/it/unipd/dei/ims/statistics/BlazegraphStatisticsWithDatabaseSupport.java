package it.unipd.dei.ims.statistics;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.jena.ext.com.google.common.base.Stopwatch;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.TupleQueryResult;
import org.openrdf.repository.Repository;
import org.openrdf.repository.RepositoryConnection;
import org.openrdf.repository.RepositoryException;

import com.bigdata.journal.Options;
import com.bigdata.rdf.sail.BigdataSail;
import com.bigdata.rdf.sail.BigdataSailRepository;

import it.unipd.dei.ims.terrier.utilities.BlazegraphUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.MapsUsefulMethods;
import it.unipd.dei.ims.terrier.utilities.SQLUtilities;
import it.unipd.dei.ims.terrier.utilities.UrlUtilities;

/**This class utilize Blazegraph as support library to deal with RDF and JDBC
 * to save information in memory.
 * */
public class BlazegraphStatisticsWithDatabaseSupport {
	
	private int edgeSetCardinality;
	
	/**This map keeps track of the frequencies of the labels in the graph.*/
	private Map<String, Integer> labelCounterMap;
	
	/**This map helps keeping track of the in degree of the nodes
	 * */
	private Map<String, Integer> inDegreeMap;
	
	/**This map helps keeping track of the out degree
	 * */
	private Map<String, Integer> outDegreeMap;
	
	/** Sql query to retrieve labels in LABEL tabel above a certain threshold.
	 * */
	private static final String SQL_SELECT_CONNECTING_LABELS = "SELECT label_name from LABEL WHERE frequency > ?  LIMIT ?";
	
	
	
	public BlazegraphStatisticsWithDatabaseSupport () {
		edgeSetCardinality = 0;
		labelCounterMap = new HashMap<String, Integer>();
		inDegreeMap = new HashMap<String, Integer>();
		outDegreeMap = new HashMap<String, Integer>();
	}
	
	/** Crates the statistics of a graph, saving them in memory.
	 * 
	 * 
	 * @param rdfDatabase Path to the RDF database to query
	 * @param jdbcDatabase path to the database to be used. An example is: 
	 * jdbc:postgresql://localhost:5432/disgenet?user=postgres&password=password
	 * @param literalFlag when set to true, count the literals as neighbours when counting
	 * the out degree of a node and doesn't count the labels on the predicates.
	 * */
	public void getGraphStatistics(String rdfDataset, String jdbcDatabase, boolean literalFlag) {
		
		Connection connection = null;
		RepositoryConnection cxn = null;
		
		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcDatabase);
			
			//open connection to RDF dataset
			Properties props = new Properties();
			props.put(Options.BUFFER_MODE, "DiskRW");
			props.put(Options.FILE, rdfDataset);
			final BigdataSail sail = new BigdataSail(props); // instantiate a sail
			final Repository repo = new BigdataSailRepository(sail); // create a Sesame repository
			//open the connection to the database (remember to shut it off at the end)
			repo.initialize();
			cxn = BlazegraphUsefulMethods.getRepositoryConnection(repo);
			
			//get an iterator over the RDF dataset 
			TupleQueryResult iterator = BlazegraphUsefulMethods.getIterator(cxn);
			System.out.println("Got iterator over RDF dataset. Start navigating...");
			
			int progressCounter = 0;
			Stopwatch timer = Stopwatch.createUnstarted();
			//start the iteration to collect information
			
			while(iterator.hasNext()) {
				edgeSetCardinality++;
				progressCounter++;
				//get the triple
				BindingSet bs = iterator.next();
				
				//take subject, predicate and object
				Value subject = bs.getValue("s");
				Value predicate = bs.getValue("p");
				Value object = bs.getValue("o");
				
				//update the label counter
				if(literalFlag) {
					MapsUsefulMethods.updateSupportMap(predicate.toString(), this.labelCounterMap);
				}

				//update in degree and out degree of nodes
				//the subject has +1 as out degree, the object +1 as in degree
				if(literalFlag) {
					//if literalFlag is true, we update anyway, both when the subject is a IRI and when it is a Lieral
					MapsUsefulMethods.updateSupportMap(subject.toString(), this.outDegreeMap);
				}
				else if(object instanceof URI) {
					MapsUsefulMethods.updateSupportMap(subject.toString(), this.outDegreeMap);
				}
				
				MapsUsefulMethods.updateSupportMap(object.toString(), this.inDegreeMap);
				
				//update the database
				if(progressCounter >= 100000) {
					progressCounter = 0;
					timer.start();
					System.out.print("Read " + this.edgeSetCardinality + " triples. Updating the database...");
					if(literalFlag) {
						//if literal flag we update as usual
						SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
						System.out.println("Updated labels in " + timer);
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated nodes in " + timer.stop());
					}
					else {
						//else, we only update the iri out degree
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated iri out degree in nodes in " + timer.stop());
					}
					timer.reset();
				}
			}//end of iteration over the edges
			
			if(progressCounter > 0) {
				//one last time
				System.out.println("last update...");
				if(literalFlag) {
					SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
				}
				SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
			}
			//clean-up 
			iterator.close();
			
			
		} catch (SQLException e) {
			
			System.err.println("DEBUG: SQL error");
			e.printStackTrace();
		
		} catch (RepositoryException e) {
			System.err.println("error initializing RDF connection to database");
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			System.err.println("Debug: error in valiuating hasNext() on the TupleQueryResult iterator");
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
				if(cxn != null) {
					cxn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				System.err.println("Error in closing RDF connection");
				e.printStackTrace();
			}
		}
	}
	
	/** Explores a graph contained as triple store in ad RDF database and creates the
	 * statistics necessary to the cluster algorithm, also storing them in a database.
	 *
	 * */
	public void getGraphStatisticsFast(String jdbcDatabase, String jdbcDriver, boolean literalFlag) {
		
		//prepare RDB connection
		try {
			Class.forName(jdbcDriver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection connection = null;
		
		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcDatabase);
			
			//get an iterator over the RDF dataset 
			ResultSet rs = this.getTripleIterator(connection);
			System.out.println("Got iterator over RDF dataset. Start navigating...");
			
			int progressCounter = 0;
			Stopwatch timer = Stopwatch.createUnstarted();
			//start the iteration to collect information
			
			while(rs.next()) {
				edgeSetCardinality++;
				progressCounter++;
				
				//take subject, predicate and object
				String s = rs.getString("subject");
				String p = rs.getString("predicate");
				String o = rs.getString("object");
				
				//update the label counter
				if(literalFlag) {
					MapsUsefulMethods.updateSupportMap(p, this.labelCounterMap);
				}

				//update in degree and out degree of nodes
				//the subject has +1 as out degree, the object +1 as in degree
				if(literalFlag) {
					//if literalFlag is true, we update anyway, both when the subject is a IRI and when it is a Lieral
					MapsUsefulMethods.updateSupportMap(s, this.outDegreeMap);
				}
				else if(UrlUtilities.checkIfValidURL(o)) {
					//update only if the object is a url
					MapsUsefulMethods.updateSupportMap(s, this.outDegreeMap);
				}
				
				MapsUsefulMethods.updateSupportMap(o, this.inDegreeMap);
				
				//update the database
				if(progressCounter >= 100000) {
					progressCounter = 0;
					timer.start();
					System.out.print("Read " + this.edgeSetCardinality + " triples. Updating the database...");
					if(literalFlag) {
						//if literal flag we update as usual
						SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
						System.out.println("Updated labels in " + timer);
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated nodes in " + timer.stop());
					}
					else {
						//else, we only update the iri out degree
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated iri out degree in nodes in " + timer.stop());
					}
					timer.reset();
				}
			}//end of iteration over the edges
			
			if(progressCounter > 0) {
				//one last time
				System.out.println("last update...");
				if(literalFlag) {
					SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
				}
				SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
			}
			//clean-up 
			
			
		} catch (SQLException e) {
			
			System.err.println("DEBUG: SQL error");
			e.printStackTrace();
		
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Explores a graph contained as triple store in ad RDF database and creates the
	 * statistics necessary to the cluster algorithm, also storing them in a database.
	 *  */
	public void getGraphStatisticsOptimizedFast(String jdbcDatabase, String jdbcDriver, boolean literalFlag) {
		
		//prepare RDB connection
//		try {
//			Class.forName(jdbcDriver);
//		} catch (ClassNotFoundException e) {
//			e.printStackTrace();
//		}
		Connection connection = null;
		
		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcDatabase);
			
			//iterator over the database
			//offset over the database
			int offset = 0;
			int limit = 100000;
			
			while(true) {
				ResultSet rs = this.getTripleIteratorWithOffset(connection, limit, offset);
				//update the window 
				offset = offset + limit;
				//check if there is something inside the answer
				
				Stopwatch timer = Stopwatch.createUnstarted();
				
				if(rs.next()) {
					//if we still have blocks to read
					
					//put the cursor to the beginning of the block
					rs.absolute(0);
					//read through this block
					while(rs.next()) {
						edgeSetCardinality++;
						
						//take subject, predicate and object
						String s = rs.getString("subject");
						String p = rs.getString("predicate");
						String o = rs.getString("object");
						
						//update the label counter
						if(literalFlag) {
							MapsUsefulMethods.updateSupportMap(p, this.labelCounterMap);
						}
						
						//update in degree and out degree of nodes
						//the subject has +1 as out degree, the object +1 as in degree
						if(literalFlag) {
							//if literalFlag is true, we update anyway, both when the subject is a IRI and when it is a Lieral
							MapsUsefulMethods.updateSupportMap(s, this.outDegreeMap);
						} else if(UrlUtilities.checkIfValidURL(o)) {
							//update only if the object is a url
							MapsUsefulMethods.updateSupportMap(s, this.outDegreeMap);
						}
						
						MapsUsefulMethods.updateSupportMap(o, this.inDegreeMap);
					}//read all the block
					//updating the block in memory
					
					timer.start();
					System.out.print("Read " + this.edgeSetCardinality + " triples. Updating the database...");
					if(literalFlag) {
						//if literal flag we update as usual
						SQLUtilities.batchUpdateLabelFrequencyIntoDatabaseFromMap(connection, this.labelCounterMap);
						System.out.println("Updated labels in " + timer);
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated nodes in " + timer.stop());
					} else {
						//else, we only update the iri out degree
						SQLUtilities.batchUpdateDegreeInDatabaseFromMaps(connection, this.outDegreeMap, this.inDegreeMap, literalFlag);
						System.out.println("Updated iri out degree in nodes in " + timer.stop());
					}
					timer.reset();
				} 
				else
					//we have finished our trip, seen all the triples, lets get out of here
					break;
			}
		} catch (SQLException e) {
			System.err.println("DEBUG: SQL error");
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Interrogates the database represented by the connection parameter
	 * to get a ResultSet representing all the triples in the triple_store
	 * table
	 * @return 
	 * @throws SQLException 
	 * */
	private ResultSet getTripleIterator(Connection connection) throws SQLException {
		String sql = "SELECT subject, predicate, object" + 
				"	FROM public.triple_store;";
		
		PreparedStatement stmt = connection.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		return rs;
	}
	
	/** Interrogates the database represented by the connection parameter
	 * to get a ResultSet representing all the triples in the triple_store
	 * table
	 * @return 
	 * @throws SQLException 
	 * */
	private ResultSet getTripleIteratorWithOffset(Connection connection, int limit, int offset) throws SQLException {
		String sql = "SELECT subject, predicate, object" + 
				"	FROM public.triple_store order by subject LIMIT ? OFFSET ?;";
		
		PreparedStatement stmt = connection.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_UPDATABLE);
		stmt.setInt(1, limit);
		stmt.setInt(2, offset);
		ResultSet rs = stmt.executeQuery();
		
		return rs;
	}
	
	/** Reading the database, gets the list of labels that are deemed relevant to be
	 * explored.
	 * 
	 * */
	public static List<String> getKConnectivityList (String jdbcDatabase, int threshold, int k) {
		List<String> list = new ArrayList<String>();
		//open the connection to the database
		//prepare RDB connection
		
		Connection connection = null;
		
		try {
			//open RDB connection
			connection = DriverManager.getConnection(jdbcDatabase);
			//perform query
			PreparedStatement preparedSelect = connection.prepareStatement(SQL_SELECT_CONNECTING_LABELS);
			preparedSelect.setInt(1, threshold);
			preparedSelect.setInt(2, k);
			
			ResultSet rs = preparedSelect.executeQuery();
			//get the strings and put them in the list
			while(rs.next()) {
				String value = rs.getString(1);
				list.add(value);
			}
		
		} catch (SQLException e) {
			System.err.println("DEBUG: SQL error");
			e.printStackTrace();
		} finally {
			try {
				if (connection != null)
					connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			} 
		}
		
		return list;
	}
	
	
	
	/** Thread to monitor how fast the algorithm is going.
	 * 
	 * */
	private class MonitorStatisticsThread extends Thread {
		public void run() {
			System.out.println("Read " + BlazegraphStatisticsWithDatabaseSupport.this.edgeSetCardinality + " edges");
		}
	}
}
